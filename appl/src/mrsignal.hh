// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_MSBERN_PROBLEM_MRSIGNAL_HH
#define DUMUX_MSBERN_PROBLEM_MRSIGNAL_HH

#include <dune/common/timer.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dumux/io/gnuplotinterface.hh>
#include <dumux/io/container.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/multidomain/glue.hh>

#if HAVE_TBB
#include <tbb/tbb.h>
#endif

namespace Dumux {

template <class MDTraits>
class MRSignalProcessor
{
    using Scalar = typename MDTraits::Scalar;
    using SolutionVector = typename MDTraits::SolutionVector;

    static constexpr auto tissueIdx = typename MDTraits::template SubDomain<0>::Index();
    static constexpr auto bloodIdx = typename MDTraits::template SubDomain<1>::Index();

    // the sub domain type tags
    template<std::size_t id>
    using SubDomainTypeTag = typename MDTraits::template SubDomain<id>::TypeTag;

    template<std::size_t id> using Problem = GetPropType<SubDomainTypeTag<id>, Properties::Problem>;
    template<std::size_t id> using GridVariables = GetPropType<SubDomainTypeTag<id>, Properties::GridVariables>;
    template<std::size_t id> using ElementVolumeVariables = typename GetPropType<SubDomainTypeTag<id>, Properties::GridVolumeVariables>::LocalView;
    template<std::size_t id> using GridGeometry = GetPropType<SubDomainTypeTag<id>, Properties::GridGeometry>;
    template<std::size_t id> using GridView = typename GridGeometry<id>::GridView;
    template<std::size_t id> using ElementMapper = typename GridGeometry<id>::ElementMapper;
    template<std::size_t id> using Element = typename GridView<id>::template Codim<0>::Entity;

    using GlobalPosition = typename Element<tissueIdx>::Geometry::GlobalCoordinate;
    using GlueType = MultiDomainGlue<GridView<tissueIdx>, GridView<bloodIdx>, ElementMapper<tissueIdx>, ElementMapper<bloodIdx>>;

    static constexpr int dim = GridView<tissueIdx>::dimension;
    static constexpr int dimWorld = GridView<tissueIdx>::dimensionworld;
    static constexpr int phaseIdx = 0;
    static constexpr int compIdx = 0;

    struct Segment {
        GlobalPosition c0, c1;
        Scalar volume;
        Scalar radius;
    };

    std::vector<Segment> segments;
    std::vector<std::size_t> segmentIndex_;
    std::vector<std::size_t> isNeighbor_;
public:
    template<class GridData>
    MRSignalProcessor(std::shared_ptr<const Problem<tissueIdx>> tissueProblem,
                      std::shared_ptr<const Problem<bloodIdx>> bloodProblem,
                      std::shared_ptr<GridData> gridData)
    : tissueProblem_(tissueProblem)
    , bloodProblem_(bloodProblem)
    , writer_(tissueProblem->gridGeometry().gridView(), "T2", "", "")
    , glue_()
    {
        S0 = getParam<Scalar>("MRSignal.S0");
        r1 = getParam<Scalar>("MRSignal.r1");
        r2 = getParam<Scalar>("MRSignal.r2");
        TE = getParam<Scalar>("MRSignal.TE");
        TR = getParam<Scalar>("MRSignal.TR");
        kappaB = getParam<Scalar>("MRSignal.KappaB");
        kappaT = getParam<Scalar>("MRSignal.KappaT");
        R1Pre = 1.0/getParam<Scalar>("MRSignal.T1TissuePreContrast");
        R2Pre = 1.0/getParam<Scalar>("MRSignal.T2TissuePreContrast");
        enableVtkOutput = getParam<bool>("Problem.PlotSignal");

        glue_.build(tissueProblem->gridGeometry().boundingBoxTree(),
                    bloodProblem->gridGeometry().boundingBoxTree());

        // allocate some vector for later
        const auto& tissueGridView = tissueProblem->gridGeometry().gridView();
        const auto numTissueElements = tissueGridView.size(0);

        // the vector of R1/R2 relaxation rates for each tissue element
        // initialize with pre constrast relaxation rates
        R2.resize(numTissueElements, R2Pre);
        R1.resize(numTissueElements, R1Pre);
        // the concentration in the interstitial space (useful to cache for later)
        cI.resize(numTissueElements);
        localConcentrationDiff.resize(glue_.size());
        isecVolume.resize(glue_.size());

        // first cache some segment data for effeciency
        const auto& bloodGridView = bloodProblem_->gridGeometry().gridView();
        const auto level0GridView = bloodGridView.grid().levelGridView(0);
        segments.resize(level0GridView.size(0));
        for (const auto& element : elements(level0GridView))
        {
            const unsigned int sIdx = level0GridView.indexSet().index(element);
            const auto segmentGeometry = element.geometry();
            segments[sIdx].c0 = segmentGeometry.corner(0);
            segments[sIdx].c1 = segmentGeometry.corner(1);
            const auto radius = gridData->parameters(element)[0];
            segments[sIdx].volume = segmentGeometry.volume()*M_PI*radius*radius;
            segments[sIdx].radius = radius;
        }

        segmentIndex_.resize(glue_.size());
        isNeighbor_.resize(glue_.size());
        int intersectionIdx = 0;
        for (const auto& is : intersections(glue_))
        {
            auto inside = is.targetEntity(0); // all 1d neighor entities are identical
            while (inside.hasFather())
                inside = inside.father();

            segmentIndex_[intersectionIdx] = level0GridView.indexSet().index(inside);
            isNeighbor_[intersectionIdx] = is.numDomainNeighbors();
            intersectionIdx++;
        }

        // For each 3d element compute distance to every 1d segments (level 0)
        auto phi = [](const Scalar& R, const Scalar& r){ return r <= R ? 1.0 : R*R/(r*r); };
        weights_.resize(numTissueElements);
        for (const auto& element : elements(tissueGridView))
        {
            const auto eIdx = tissueProblem_->gridGeometry().elementMapper().index(element);
            const auto center = element.geometry().center();
            weights_[eIdx].resize(segments.size());
            for (auto sIdx = 0; sIdx < segments.size(); ++sIdx)
            {
                const auto& segment = segments[sIdx];
                const auto dist = distanceToSegment(center, segment.c0, segment.c1);
                if(dist < 0)
                    DUNE_THROW(Dune::InvalidStateException, "Negative distance!");
                weights_[eIdx][sIdx] = phi(segment.radius, dist)/segment.volume;
            }
        }

        // for debugging plot the weights
        weightPlot_.resize(weights_.size(), 0.0);
        for (int i = 0; i < weightPlot_.size(); ++i)
            weightPlot_[i] = std::log10(std::accumulate(weights_[i].begin(), weights_[i].end(), 0.0));

        // resize the current signal vector
        localSignal_.resize(numTissueElements, 0.0);
        localT2Star_.resize(numTissueElements, 0.0);
        localT1_.resize(numTissueElements, 0.0);
        concentrations_.resize(numTissueElements, 0.0);
        writer_.addCellData(weightPlot_, "distance weights");
        writer_.addCellData(localT2Star_, "T2*");
        writer_.addCellData(localT1_, "T1");
        writer_.addCellData(localSignal_, "signal");
        writer_.addCellData(concentrations_, "c");
    }

    template<class CouplingManager>
    void generateSignal(Scalar time, const SolutionVector& sol,
                        const GridVariables<tissueIdx>& tissueGridVars,
                        const GridVariables<bloodIdx>& bloodGridVars,
                        const CouplingManager& couplingManager)
    {
        // generate signal
#if HAVE_TBB
        const auto t0 = tbb::tick_count::now();
#else
        Dune::Timer timer;
#endif
        generateMRSignalLocal(time, sol, tissueGridVars, bloodGridVars, couplingManager);
#if HAVE_TBB
        const auto t1 = tbb::tick_count::now();
        std::cout << "Generated MR signal in " << (t1-t0).seconds() << " seconds (parallel version).\n";
#else
        std::cout << "Generated MR signal in " << timer.elapsed() << " seconds (sequential version).\n";
#endif
    }

    //! get signal given a time vector
    std::vector<Scalar> getSignal(const std::vector<Scalar>& t)
    {
        auto normalizedSignal = voxelSignals_;
        const auto firstSignal = voxelSignals_[0];
        for (auto&& s : normalizedSignal)
            s /= firstSignal;

        std::vector<Scalar> s(t.size(), 0.0);
        for (std::size_t i = 0; i < t.size(); ++i)
            s[i] = interpolateSolutionAtTime(normalizedSignal, time_, t[i]);

        return s;
    }

    //! plot signal using gunplot
    void plotSignal()
    {
        std::vector<Scalar> t(80, 1.4); t[0] = 0.0;
        std::partial_sum(t.begin(), t.end(), t.begin());
        auto s = getSignal(t);

        const std::string outputDir = getParam<std::string>("Problem.GnuplotOutputDirectory");
        const bool openGnuPlot = getParam<bool>("Problem.GnuplotShow");
        GnuplotInterface<Scalar> gnuplot(true);
        gnuplot.resetPlot();
        gnuplot.setOpenPlotWindow(openGnuPlot);
        gnuplot.setOutputDirectory(outputDir);
        gnuplot.setXlabel("time in s");
        gnuplot.setYlabel("voxel MR signal (normalized)");
        gnuplot.addDataSetToPlot(t, s, "mrsignal.dat", "title 'MR signal'");
        gnuplot.plot("mrsignal");
    }

    template<typename Vector, typename Vector2>
    static Scalar interpolateSolutionAtTime(const Vector& sol, const Vector2& timeVector, Scalar time)
    {
        assert(sol.size() == timeVector.size());
        auto it = std::lower_bound(timeVector.begin(), timeVector.end(), time);
        // Corner case
        if (it == timeVector.begin()) return sol[0];
        auto it2 = it;
        --it2;
        auto i = std::distance(timeVector.begin(), it);
        return sol[i-1] + (sol[i] - sol[i-1])*(time - *it2)/(*it - *it2);
    }

private:

    // MR signal including local susceptibility effects
    template<class CouplingManager>
    void generateMRSignalLocal(Scalar time, const SolutionVector& sol,
                               const GridVariables<tissueIdx>& tissueGridVars,
                               const GridVariables<bloodIdx>& bloodGridVars,
                               const CouplingManager& couplingManager)
    {
        const auto& tissueGridGeometry = tissueProblem_->gridGeometry();
        const auto& bloodGridGeometry = bloodProblem_->gridGeometry();
        const auto& tissueGridView = tissueGridGeometry.gridView();
        const auto numTissueElements = tissueGridView.size(0);

        // initialize with pre constrast relaxation rates
        R2.assign(numTissueElements, R2Pre);
        R1.assign(numTissueElements, R1Pre);

        // get local tissue concentrations
        static const Scalar phiI = getParam<Scalar>("SpatialParams.PorosityTissue");
        for (const auto& element : elements(tissueGridView))
        {
            const auto eIdx = tissueGridGeometry.elementMapper().index(element);

            auto fvGeometry = localView(tissueGridGeometry);
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(tissueGridVars.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, sol[tissueIdx]);

            const auto phiB = couplingManager.lowDimVolumeFraction(element);

            // there is only one scv but using the loop is easier
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                cI[eIdx] = volVars.moleFraction(phaseIdx, compIdx)
                           * volVars.molarDensity();
            }

            // add the micro-scale contribution of the contrast agent in interstitial space
            R2[eIdx] += r2*phiI*cI[eIdx];
            R1[eIdx] += r1*phiI*cI[eIdx];

            // add the meso-scale contribution of the interstitial-cell interface
            // we assume the effect is only locally important in constrast to the blood-interstitial interface
            R2[eIdx] += kappaT*cI[eIdx]*(1.0-phiB); // phiI + phiS (symmetric contributions)
        }

        concentrations_ = cI;

        // for the blood contribution to the relaxation rate
        // we need the intersections
        int intersectionIdx = 0;

        for (const auto& is : intersections(glue_))
        {
            const auto& inside = is.targetEntity(0); // all insides are identical
            const auto intersectionGeometry = is.geometry();
            isecVolume[intersectionIdx] = intersectionGeometry.volume();
            const unsigned int lowDimElementIdx = bloodGridGeometry.elementMapper().index(inside);
            const auto radius = bloodProblem_->spatialParams().radius(lowDimElementIdx);

            auto fvGeometry = localView(bloodGridGeometry);
            fvGeometry.bindElement(inside);

            auto elemVolVars = localView(bloodGridVars.curGridVolVars());
            elemVolVars.bindElement(inside, fvGeometry, sol[bloodIdx]);

            // get concentration
            // there is only one scv but using the loop is easier
            Scalar cB = 0.0;
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                cB = volVars.moleFraction(phaseIdx, compIdx)
                     * volVars.molarDensity();
            }

            // in case there are multiple 3d elements intersecting deal with all of them
            localConcentrationDiff[intersectionIdx].resize(is.numDomainNeighbors());
            for (unsigned int outsideIdx = 0; outsideIdx < is.numDomainNeighbors(); ++outsideIdx)
            {
                const auto& outside = is.domainEntity(outsideIdx);
                const unsigned int bulkElementIdx = tissueGridGeometry.elementMapper().index(outside);
                const Scalar bloodVolume = isecVolume[intersectionIdx]*M_PI*radius*radius/is.numDomainNeighbors();
                const Scalar localPhiB = bloodVolume / outside.geometry().volume();

                // add the micro-scale contribution of the contrast agent in blood
                R2[bulkElementIdx] += r2*localPhiB*cB;
                R1[bulkElementIdx] += r1*localPhiB*cB;

                // add the meso-scale effect of the blood-interstitial interface in the blood compartment
                R2[bulkElementIdx] += kappaB*localPhiB*std::abs(cB - cI[bulkElementIdx]);
                localConcentrationDiff[intersectionIdx][outsideIdx] = std::abs(cB - cI[bulkElementIdx]);
            }

            ++intersectionIdx;
        }

        // now comes the most expensive part, adding contributions from all intersections to every
        // tissue element to include the non-local meso-scale contribution of the blood-interstitial interface
        // this loop makes the most work as we need to iterate over all segments per 3d elements
        // we can easily parallelize it using intel thread building blocks
#if HAVE_TBB
        tbb::parallel_for( std::size_t(0), std::size_t(numTissueElements),
                           [&](std::size_t eIdx)
#else
        for (std::size_t eIdx = 0; eIdx < std::size_t(numTissueElements); ++eIdx)
#endif
        {
            Scalar weightedConcentrationDiff = 0.0;
            for (int intersectionIdx = 0; intersectionIdx < glue_.size(); ++intersectionIdx)
            {
                const auto intersectionLength = isecVolume[intersectionIdx];
                const auto sIdx = segmentIndex_[intersectionIdx];
                const auto& weight = weights_[eIdx][sIdx];
                const auto radius = segments[sIdx].radius;

                for (unsigned int outsideIdx = 0; outsideIdx < isNeighbor_[intersectionIdx]; ++outsideIdx)
                    weightedConcentrationDiff += localConcentrationDiff[intersectionIdx][outsideIdx]
                                                 * intersectionLength/isNeighbor_[intersectionIdx] * radius * radius * M_PI * weight;
            }

            // add the meso-scale effect of the blood-interstitial interface in the interstitial compartment
            R2[eIdx] += phiI*kappaB*weightedConcentrationDiff;

            // compute local T2* and T1 for output
            if (enableVtkOutput)
            {
                localT2Star_[eIdx] = 1.0/R2[eIdx];
                localT1_[eIdx] = 1.0/R1[eIdx];
            }

            // compute the signal for this tissue element
            localSignal_[eIdx] = S0*(1.0 - std::exp(-TR*R1[eIdx]))*std::exp(-TE*R2[eIdx]);

#if HAVE_TBB
        }); // end parallel for
#else
        } // end sequential for
#endif

        // compute the accumulated signal over the whole domain (i.e. one voxel)
        voxelSignals_.push_back(std::accumulate(localSignal_.begin(), localSignal_.end(), 0.0));
        time_.push_back(time);

        // plot the spatial distribution of the local T2*s
        if(enableVtkOutput) writer_.write(time_.back());
    }

    // Compute the distance of p to the segment connecting a->b
    static Scalar distanceToSegment(const GlobalPosition& p,
                                    const GlobalPosition& a,
                                    const GlobalPosition& b)
    {
        const auto v = b - a;
        const auto w = p - a;

        const auto proj1 = v*w;
        if (proj1 <= 0.0)
            return w.two_norm();

        const auto proj2 = v.two_norm2();
        if (proj2 <= proj1)
            return (p - b).two_norm();

        const auto t = proj1 / proj2;
        auto x = a;
        x.axpy(t, v);
        return (x - p).two_norm();
    }

    std::shared_ptr<const Problem<tissueIdx>> tissueProblem_;
    std::shared_ptr<const Problem<bloodIdx>> bloodProblem_;
    Dune::VTKSequenceWriter<GridView<tissueIdx>> writer_;
    GlueType glue_;

    //! MR signal parameters
    Scalar S0, r1, r2, TE, TR, kappaB, kappaT, R1Pre, R2Pre;
    bool enableVtkOutput;

    Scalar maxDtInjection_, initDtInjection_;
    std::vector<Scalar> voxelSignals_, localSignal_, localT2Star_, localT1_, weightPlot_, concentrations_;
    std::vector<Scalar> time_;
    std::vector<Scalar> cTissue_, cBlood_;
    std::vector<std::vector<Scalar>> weights_;

    std::vector<Scalar> R2, R1, cI, isecVolume;
    std::vector<std::vector<Scalar>> localConcentrationDiff;
};

} //end namespace Dumux

#endif
