""" Define a parser from EBNF for the meta ini syntax """
from __future__ import absolute_import
from __future__ import print_function

from pyparsing import *
import codecs

class DotDict(dict):
    def __init__(self, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)

    def __getitem__(self, key):
        key = str(key)
        if "." in key:
            group, key = key.split(".", 1)
            return dict.__getitem__(self, group)[key]
        else:
            return dict.__getitem__(self, key)

    def __setitem__(self, key, value):
        key = str(key)
        if "." in key:
            group, key = key.split(".", 1)
            if group not in self:
                dict.__setitem__(self, group, DotDict())
            dict.__getitem__(self, group).__setitem__(key, value)
        else:
            dict.__setitem__(self, key, value)

    def __contains__(self, key):
        key = str(key)
        if "." in key:
            group, key = key.split(".", 1)
            if group not in self:
                return False
            return dict.__getitem__(self, group).__contains__(key)
        else:
            return dict.__contains__(self, key)

    def __delitem__(self, key):
        key = str(key)
        if "." in key:
            group, key = key.split(".", 1)
            dict.__getitem__(self, group).__delitem__(key)
            if len(dict.__getitem__(self, group)) is 0:
                dict.__delitem__(self, group)
        else:
            dict.__delitem__(self, key)

    def __len__(self):
        return sum([1 for i in self.__iter__()])

    def __iter__(self, prefix=[]):
        for i in dict.__iter__(self):
            if type(self[i]) is DotDict:
                prefix.append(i)
                for x in self[i].__iter__(prefix):
                    yield x
                prefix.pop()
            else:
                def groupname():
                    result = ""
                    for p in prefix:
                        result = result + p + "."
                    return result
                yield groupname() + i

    def __str__(self):
        s = ""
        for k, v in list(self.items()):
            s = s + "'" + str(k) + "': '" + str(v) + "', "
        return "{" + s[:-2] + "}"

    def filter(self, filterList):
        d = DotDict()
        for k in self:
            if True in [k.startswith(f) for f in filterList]:
                d[k] = self[k]
        return d

    def __hash__(self):
        return hash(tuple(sorted(self.items())))

    def __eq__(self, other):
        return tuple(sorted(self.items())) == tuple(sorted(other.items()))

    def __lt__(self, other):
        """ make the DotDict orderable to enforce an order on a list of ini files
        This is only well-defined for dicts sharing the set of keys.
        """
        if tuple(sorted(self.keys())) != tuple(sorted(other.keys())):
            return tuple(sorted(self.keys())) < tuple(sorted(other.keys()))
        for k in sorted(self.keys()):
            if self[k] < other[k]:
                return True
            if self[k] > other[k]:
                return False

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default

    def items(self):
        return [(k, self[k]) for k in self.__iter__()]

    def keys(self):
        return [k for k in self.__iter__()]

    def values(self):
        return [self[k] for k in self.__iter__()]



class DuneIniParser(object):
    # Define a debug logging switch
    _debug = False

    def __init__(self, assignment="=", commentChar="#"):
        self._currentGroup = ''
        self._result = DotDict()
        self._parser = self.construct_bnf(assignment=assignment, commentChar=commentChar)

    def log(self, s):
        if DuneIniParser._debug:
            print(s)

    def construct_bnf(self, assignment="=", commentChar="#"):
        """ The EBNF for a normal Dune style ini file. """
        # A comment starts with the comment literal and affects the rest of the line
        comment = Literal(commentChar).suppress() + Optional(restOfLine).suppress()
        # A section is guarded by square brackets
        section = Literal("[") + Word(alphanums + "._").setParseAction(self.setGroup) + Literal("]")
        # A key can consist of anything that is not an equal sign
        key = Word(alphanums + "-_.")
        # A value may contain virtually anything
        value = Word(printables + " ", excludeChars=[commentChar])
        # A key value pair is of the form 'key=value'
        keyval = (key + Literal(assignment).suppress() + value).setParseAction(self.setKeyValuePair)
        # Define the priority between the different sorts of lines.
        content = keyval | section
        # Define a line
        line = Optional(content) + Optional(comment) + LineEnd()

        return line

    def setGroup(self, origString, loc, tokens):
        self.log("Setting current group from '{}' to '{}.'".format(self._currentGroup, tokens[0].strip()))
        self._currentGroup = tokens[0] + "."

    def setKeyValuePair(self, origString, loc, tokens):
        self.log("Setting KV pair ('{}', '{}') within group '{}'".format(tokens[0].strip(), tokens[1].strip(), self._currentGroup))
        # store the key value pair for the return dictionary
        self._result[self._currentGroup + tokens[0].strip()] = tokens[1].strip()

    def apply(self, filename):
        self.log("Parsing file: {}".format(filename))
        f = open(filename, "r", encoding="utf-8")
        for line in f:
            self.log("Parsing line: {}".format(line))
            self._parser.parseString(line)

        return self._result


def parse_ini_file(filename):
    return DuneIniParser().apply(filename)


def write_to_stream(d, stream, assignment="="):
    """Write a dictionary to stream in ini file syntax

        :param d: The dictionary representing an ini file
        :type d: DotDict, dict
        :param stream: The stream to write to
        :type stream: string
        :param assignment: The assignment operator used to connect key and value in the stream
        :type assignment: single character

    """
    def traverse_dict(stream, d, prefix):
        # first traverse all non-group values (they would otherwise be considered part of a group)
        for key, value in sorted(dict.items(d)):
            if not isinstance(value, dict):
                stream.write("{} {} {}\n".format(key, assignment, value))

        # now go into subgroups
        for key, value in sorted(dict.items(d)):
            if isinstance(value, dict):
                pre = prefix + [key]

                def groupname(prefixlist):
                    prefix = ""
                    for p in prefixlist:
                        if prefix is not "":
                            prefix = prefix + "."
                        prefix = prefix + p
                    return prefix

                stream.write("\n[{}]\n".format(groupname(pre)))
                traverse_dict(stream, value, pre)

    prefix = []
    traverse_dict(stream, d, prefix)


def write_dict_to_ini(d, filename, assignment="="):
    """ Write a (nested) dictionary to a file following the ini file syntax:

        :param d: The dictionary representing an ini file
        :type d: DotDict, dict
        :param filename: The filename of the ini file
        :type filename: string
        :param assignment: The assignment operator used to connect key and value in the stream
        :type assignment: single character
    """
    f = open(filename, 'w')
    write_to_stream(d, f, assignment=assignment)
