// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_TISSUE_SPATIAL_PARAMS_HH
#define DUMUX_TISSUE_SPATIAL_PARAMS_HH

#include <dumux/common/parameters.hh>
#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {

template<class FVGridGeometry, class Scalar>
class TissueSpatialParams
: public FVSpatialParamsOneP<FVGridGeometry, Scalar, TissueSpatialParams<FVGridGeometry, Scalar>>
{
    using ThisType = TissueSpatialParams<FVGridGeometry, Scalar>;
    using ParentType = FVSpatialParamsOneP<FVGridGeometry, Scalar, ThisType>;
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimworld = GridView::dimensionworld;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    // export permeability type
    using PermeabilityType = Scalar;

    TissueSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        permeability_ = getParam<Scalar>("SpatialParams.PermeabilityTissue");
        porosity_ = getParam<Scalar>("SpatialParams.PorosityTissue");
    }

    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return permeability_; }

    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return porosity_; }

    //! Fluid properties that are spatial params in the tracer model
    //! They can possible vary with space but are usually constants

    //! fluid density
    Scalar fluidDensity(const Element &element,
                        const SubControlVolume& scv) const
    {
        static const Scalar rho = getParam<Scalar>("1.Component.LiquidDensity");
        return rho;
    }

    //! fluid molar mass
    Scalar fluidMolarMass(const Element &element,
                          const SubControlVolume& scv) const
    {
        static const Scalar mm = getParam<Scalar>("1.Component.MolarMass");
        return mm;
    }

    /*!
     * \brief The volume source for a specified source id for the transport problem
     */
    Scalar volumeSource(std::size_t sourceID) const
    {
        return volumeSource_[sourceID];
    }

    /*!
     * \brief The volume flux for a specified scvf
     */
    template<class ElementVolumeVariables>
    Scalar volumeFlux(const Element &element,
                      const FVElementGeometry& fvGeometry,
                      const ElementVolumeVariables& elemVolVars,
                      const SubControlVolumeFace& scvf) const
    {
        return volumeFlux_[scvf.index()];
    }

    void setVolumeFlux(const std::vector<Scalar>& f)
    { volumeFlux_ = f; }

    void setVolumeSource(const std::vector<Scalar>& s)
    { volumeSource_ = s; }

private:
    Scalar permeability_;
    Scalar porosity_;

    std::vector<Scalar> volumeFlux_;
    std::vector<Scalar> volumeSource_;
};

} // end namespace Dumux

#endif
