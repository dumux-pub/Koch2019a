// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_BlOOD_FLOW_SPATIALPARAMS_HH
#define DUMUX_BlOOD_FLOW_SPATIALPARAMS_HH

#include <dumux/common/parameters.hh>
#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {

/*!
 * \ingroup OneTests
 * \brief Definition of the spatial parameters for the blood flow problem
 */
template<class GridGeometry, class Scalar>
class BloodFlowSpatialParams
: public FVSpatialParamsOneP<GridGeometry, Scalar, BloodFlowSpatialParams<GridGeometry, Scalar>>
{
    using ThisType = BloodFlowSpatialParams<GridGeometry, Scalar>;
    using ParentType = FVSpatialParamsOneP<GridGeometry, Scalar, ThisType>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    // export permeability type
    using PermeabilityType = Scalar;

    BloodFlowSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {}

    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        const Scalar r = radius(scv.dofIndex());
        const Scalar gamma = 2; // quadratic velocity profile (Poiseuille flow)
        return r*r/(2*(2+gamma));
    }

    // Return the radius of the circular pipe
    Scalar radius(std::size_t eIdxGlobal) const
    { return radius_[eIdxGlobal];}

    // Return velocity estimate read from file
    Scalar velocity_estimate(std::size_t eIdxGlobal) const
    { return velocity_estimate_[eIdxGlobal]; }

    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 1.0; }

    //! Set the radii from outside
    void setRadii(const std::vector<Scalar>& radius)
    { radius_ = radius; }

    //! Get the radii for e.g. output
    const std::vector<Scalar>& getRadii() const
    { return radius_; }

    //! Read params from dgf
    template<class GridData>
    void readGridParams(const GridData& gridData)
    {
        const auto& gg = this->gridGeometry();
        auto numElements = gg.gridView().size(0);
        radius_.resize(numElements);
        velocity_estimate_.resize(numElements);

        // gridview is a leafGridView. Parameters are only set on level 0.
        // elements have to inherit spatial parameters from their father.
        for (const auto& element : elements(gg.gridView()))
        {
            auto level0element = element;
            while(level0element.hasFather())
                level0element = level0element.father();

            auto eIdx = gg.elementMapper().index(element);
            radius_[eIdx] = gridData.parameters(level0element)[0];
            velocity_estimate_[eIdx] = gridData.parameters(level0element)[1];
        }
    }

    //! Fluid properties that are spatial params in the tracer model
    //! They can possible vary with space but are usually constants

    //! fluid density
    Scalar fluidDensity(const Element &element,
                        const SubControlVolume& scv) const
    {
        static const Scalar rho = getParam<Scalar>("2.Component.LiquidDensity");
        return rho;
    }

    //! fluid molar mass
    Scalar fluidMolarMass(const Element &element,
                          const SubControlVolume& scv) const
    {
        static const Scalar mm = getParam<Scalar>("2.Component.MolarMass");
        return mm;
    }

    /*!
     * \brief The volume source for a specified source id for the transport problem
     */
    Scalar volumeSource(std::size_t sourceID) const
    {
        return volumeSource_[sourceID];
    }

    /*!
     * \brief The volume flux for a specified scvf
     */
    template<class ElementVolumeVariables>
    Scalar volumeFlux(const Element &element,
                      const FVElementGeometry& fvGeometry,
                      const ElementVolumeVariables& elemVolVars,
                      const SubControlVolumeFace& scvf) const
    {
        return volumeFlux_[scvf.index()];
    }

    void setVolumeFlux(const std::vector<Scalar>& f)
    { volumeFlux_ = f; }

    void setVolumeSource(const std::vector<Scalar>& s)
    { volumeSource_ = s; }

private:
    std::vector<Scalar> radius_;
    std::vector<Scalar> velocity_estimate_;

    std::vector<Scalar> volumeFlux_;
    std::vector<Scalar> volumeSource_;
};

} // end namespace Dumux

#endif
