// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief compute l2 norm
 */
#include <config.h>

#include <iostream>
#include <mutex>
#include <random>
#include <tbb/tbb.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/grid/common/mcmgmapper.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/container.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/math.hh>
#include <dumux/common/entitymap.hh>
#include <dumux/common/geometry/boundingboxtree.hh>
#include <dumux/common/geometry/geometricentityset.hh>
#include <dumux/common/geometry/intersectingentities.hh>

namespace Dumux {

template<class Scalar>
class UniformDistributedRandomNumber
{
public:
    UniformDistributedRandomNumber(Scalar min, Scalar max)
    : gen_(r_()), dist_(min, max) {}

    Scalar operator()()
    {
        return dist_(gen_);
    }

private:
    std::random_device r_;
    std::default_random_engine gen_;
    std::uniform_real_distribution<Scalar> dist_;
};

struct PointGenerator
{
    using GlobalCoordinate = Dune::FieldVector<double, 3>;

    GlobalCoordinate
    gen(const GlobalCoordinate& origin,
        const GlobalCoordinate& direction,
        double radius)
    {
        GlobalCoordinate p({uni01_(), uni01_(), uni01_()});
        // get perpendicular vector
        auto a = crossProduct(direction, p);
        a /= a.two_norm();

        auto rFactor = std::sqrt(uni01_());
        auto lFactor = uni01_();

        auto point = origin;
        point.axpy(lFactor, direction);
        point.axpy(rFactor*radius, a);

        return point;
    }

private:
    UniformDistributedRandomNumber<double> uni11_{-1.0, 1.0};
    UniformDistributedRandomNumber<double> uni01_{0.0, 1.0};
};

template<class GridView>
std::vector<bool> findElementsContainedInVessels(const GridView& gridView)
{
    std::vector<bool> exclude(gridView.size(0), false);

    const std::size_t samples = getParam<std::size_t>("L2Norm.ExclusionSamplingSize", 1<<15);
    PointGenerator pointGenerator;

    // read the vessel grid with data
    GridManager<Dune::FoamGrid<1,3>> vesselGridManager;
    vesselGridManager.init("Vessel"); // pass parameter group
    auto gridData = vesselGridManager.getGridData();

    using ElementSet = GridViewGeometricEntitySet<GridView, 0>;
    using BoundingBoxTree = Dumux::BoundingBoxTree<ElementSet>;
    auto boundingBoxTree = std::make_unique<BoundingBoxTree>(std::make_shared<ElementSet>(gridView));

    // iterate over elements and find 3d element in the cylinders
    // to exclude by using uniformly distributed points
    // and the bounding box tree, do this search thread-parallel
    std::mutex excludeMutex;
    for (const auto& element : elements(vesselGridManager.grid().leafGridView()))
    {
        const auto geometry = element.geometry();
        const auto origin = geometry.corner(0);
        const auto direction = geometry.corner(1)-origin;
        const auto radius = gridData->parameters(element)[0];
        tbb::parallel_for (std::size_t(0), std::size_t(samples), [&](std::size_t i)
        {
            const auto point = pointGenerator.gen(origin, direction, radius);
            const auto entities = intersectingEntities(point, *boundingBoxTree, /*isCartesianGrid=*/true);
            for (const auto eIdx : entities)
            {
                std::lock_guard<std::mutex> lock(excludeMutex);
                exclude[eIdx] = true;
            }
        });
    }

    return exclude;
}

} // end namespace Dumux

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    Dune::MPIHelper::instance(argc, argv);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    using Grid = Dune::YaspGrid<3, Dune::EquidistantOffsetCoordinates<double, 3>>;
    GridManager<Grid> gridManager; gridManager.init();
    auto& grid = gridManager.grid();

    const auto maxLevel = grid.maxLevel();
    std::cout << "maxLevel: " << maxLevel << std::endl;

    std::mutex maxNormMutex; // protect maxNorms from data races
    std::vector<double> maxNorms(maxLevel+1, 0.0); // maximum l2 norm over time
    std::vector<double> maxNormsExclude(maxLevel+1, 0.0); // maximum l2 norm over time (with cut elements excluded)

    const auto files = getParam<std::vector<std::string>>("L2Norm.Files");
    const auto numTimeSteps = getParam<int>("L2Norm.NumTimeSteps", 0);

    if (maxLevel+1 != files.size())
        std::cout << "number of refinements and number of files (" << files.size() << ") does not match!" << std::endl;

    // a marker for those elements not considered in the norm
    const auto excludeElement = findElementsContainedInVessels(grid.leafGridView());

    auto computeNorm = [&](std::size_t t)
    {
        std::vector<std::vector<double>> sol(maxLevel+1, std::vector<double>());
        std::cout << "\nRead solutiom from ";
        for (int level = 0; level <= maxLevel; ++level)
        {
            const auto fileName = (t == 0) ? files[level] + ".out" : files[level] + "-" + std::to_string(t) + ".out";
            sol[level] = readFileToContainer<std::vector<double>>(fileName);
            std::cout << fileName << ", ";
        }
        std::cout << std::endl;

        using LevelMapper = Dune::MultipleCodimMultipleGeomTypeMapper<typename Grid::LevelGridView>;
        LevelMapper fineMapper(grid.levelGridView(maxLevel), Dune::mcmgElementLayout());

        for (int level = 0; level <= maxLevel; ++level)
            if (grid.levelGridView(level).size(0) != sol[level].size())
                std::cout << "number of elements and data size (" << grid.levelGridView(level).size(0)
                          << " != "<< sol[level].size() << ") does not match!" << std::endl;

        // compute l2-norm for all levels
        std::vector<double> norms(maxLevel+1, 0.0);
        std::vector<double> normsExclude(maxLevel+1, 0.0);
        for (int level = 0; level <= maxLevel; ++level)
        {
            std::size_t numElements = 0;
            LevelMapper levelMapper(grid.levelGridView(level), Dune::mcmgElementLayout());
            for (const auto& element : elements(grid.leafGridView()))
            {
                const auto fineIdx = fineMapper.index(element);

                auto father = element;
                for (int i = 0; i < maxLevel - level; ++i)
                    father = father.father();

                assert(father.level() == level);

                const auto levelIdx = levelMapper.index(father);
                const auto fineSol = sol[maxLevel][fineIdx];
                const auto levelSol = level == maxLevel ? 0 : sol[level][levelIdx];

                norms[level] += (fineSol-levelSol)*(fineSol-levelSol);

                if (!excludeElement[fineIdx])
                {
                    normsExclude[level] += norms[level];
                    ++numElements;
                }
            }

            norms[level] /= grid.leafGridView().size(0);
            normsExclude[level] /= numElements;
            norms[level] = std::sqrt(norms[level]);
            normsExclude[level] = std::sqrt(normsExclude[level]);

            {
                std::lock_guard<std::mutex> guard(maxNormMutex);
                maxNorms[level] = std::max(maxNorms[level], norms[level]);
                maxNormsExclude[level] = std::max(maxNormsExclude[level], normsExclude[level]);
            }
        }

        // interpolate fine scale solution to all levels
        // auto fineSol = sol;
        // for (int level = maxLevel; level > 0; --level)
        // {
        //     LevelMapper mapper(grid.levelGridView(level), Dune::mcmgElementLayout());
        //     LevelMapper fatherMapper(grid.levelGridView(level-1), Dune::mcmgElementLayout());
        //     fineSol[level-1].assign(grid.levelGridView(level-1).size(0), 0.0);
        //     for (const auto& element : elements(grid.levelGridView(level)))
        //     {
        //         auto father = element.father();
        //         const auto eIdx = mapper.index(element);
        //         const auto fatherIdx = fatherMapper.index(father);
        //         fineSol[level-1][fatherIdx] += fineSol[level][eIdx]/8.0;
        //     }
        // }
        //
        // std::vector<double> norms2(maxLevel+1, 0.0);
        // for (int level = 0; level <= maxLevel; ++level)
        // {
        //     LevelMapper levelMapper(grid.levelGridView(level), Dune::mcmgElementLayout());
        //     for (const auto& element : elements(grid.levelGridView(level)))
        //     {
        //         const auto eIdx = levelMapper.index(element);
        //         const auto fine = fineSol[level][eIdx];
        //         const auto coarse = level == maxLevel ? 0.0 : sol[level][eIdx];
        //
        //         norms2[level] += (fine-coarse)*(fine-coarse);
        //     }
        //     norms2[level] /= grid.levelGridView(level).size(0);
        //     norms2[level] = std::sqrt(norms2[level]);
        //
        //     {
        //         std::lock_guard<std::mutex> guard(maxNormMutex);
        //         maxNorms[level] = std::max(maxNorms[level], norms2[level]);
        //     }
        //
        //     // std::cout << "level: " << level << " error: " << norms2[level];
        //     // if (level > 0)
        //     // {
        //     //     const auto rate = (std::log(norms2[level-1])-std::log(norms2[level]))/(std::log(1.0/(4*(1<<(level-1))))-std::log(1.0/(4*(1<<(level)))));
        //     //     std::cout << " rate: " << rate;
        //     // }
        //     // std::cout << std::endl;
        // }
    };

    tbb::task_scheduler_init tbbInit;
    std::cout << "Available threads: " << tbb::task_scheduler_init::default_num_threads() << std::endl;

    if (numTimeSteps > 0)
        tbb::parallel_for( std::size_t(1), std::size_t(numTimeSteps), computeNorm);
    else
        computeNorm(0);

    std::cout << "Errors and rates (full domain)" << std::endl;
    for (int level = 0; level < maxLevel; ++level)
    {
        std::cout << "level: " << level << " rel error: " << maxNorms[level]/maxNorms[maxLevel];
        if (level > 0)
        {
            const auto rate = (std::log(maxNorms[level-1])-std::log(maxNorms[level]))
                              / (std::log(1.0/(5*(1<<(level-1))))-std::log(1.0/(5*(1<<(level)))));
            std::cout << " rate: " << rate;
        }
        std::cout << std::endl;
    }

    std::cout << "Errors and rates (cut elements excluded)" << std::endl;
    for (int level = 0; level < maxLevel; ++level)
    {
        std::cout << "level: " << level << " rel error: " << maxNormsExclude[level]/maxNormsExclude[maxLevel];
        if (level > 0)
        {
            const auto rate = (std::log(maxNormsExclude[level-1])-std::log(maxNormsExclude[level]))
                              / (std::log(1.0/(5*(1<<(level-1))))-std::log(1.0/(5*(1<<(level)))));
            std::cout << " rate: " << rate;
        }
        std::cout << std::endl;
    }

    return 0;
}
/////////////////////////////////////////////
///////////// Error handler /////////////////
/////////////////////////////////////////////
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
