// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief test for the ms bern problem
 */
#include <config.h>

#include <ctime>
#include <iostream>

#include <valgrind/callgrind.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/container.hh>
#include <dumux/adaptive/markelements.hh>

#include <dumux/multidomain/traits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/embedded/couplingmanager1d3d.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include "../src/bloodflowproblem.hh"
#include "../src/tissueproblem.hh"

#include "../src/bloodtransportproblem.hh"
#include "../src/tissuetransportproblem.hh"

#include "../src/mrsignal.hh"

namespace Dumux {
namespace Properties {

template<class Traits>
using TheCouplingManager = EmbeddedCouplingManager1d3d<Traits, EmbeddedCouplingMode::average>;

SET_TYPE_PROP(TissueCCTypeTag, CouplingManager, TheCouplingManager<MultiDomainTraits<TypeTag, TTAG(BloodFlowCCTypeTag)>>);
SET_TYPE_PROP(TissueCCTypeTag, PointSource, typename GET_PROP_TYPE(TypeTag, CouplingManager)::PointSourceTraits::template PointSource<0>);
SET_TYPE_PROP(TissueCCTypeTag, PointSourceHelper, typename GET_PROP_TYPE(TypeTag, CouplingManager)::PointSourceTraits::template PointSourceHelper<0>);

SET_TYPE_PROP(BloodFlowCCTypeTag, CouplingManager, TheCouplingManager<MultiDomainTraits<TTAG(TissueCCTypeTag), TypeTag>>);
SET_TYPE_PROP(BloodFlowCCTypeTag, PointSource, typename GET_PROP_TYPE(TypeTag, CouplingManager)::PointSourceTraits::template PointSource<1>);
SET_TYPE_PROP(BloodFlowCCTypeTag, PointSourceHelper, typename GET_PROP_TYPE(TypeTag, CouplingManager)::PointSourceTraits::template PointSourceHelper<1>);

SET_TYPE_PROP(TissueTransportCCTypeTag, CouplingManager, TheCouplingManager<MultiDomainTraits<TypeTag, TTAG(BloodTransportCCTypeTag)>>);
SET_TYPE_PROP(TissueTransportCCTypeTag, PointSource, typename GET_PROP_TYPE(TypeTag, CouplingManager)::PointSourceTraits::template PointSource<0>);
SET_TYPE_PROP(TissueTransportCCTypeTag, PointSourceHelper, typename GET_PROP_TYPE(TypeTag, CouplingManager)::PointSourceTraits::template PointSourceHelper<0>);

SET_TYPE_PROP(BloodTransportCCTypeTag, CouplingManager, TheCouplingManager<MultiDomainTraits<TTAG(TissueTransportCCTypeTag), TypeTag>>);
SET_TYPE_PROP(BloodTransportCCTypeTag, PointSource, typename GET_PROP_TYPE(TypeTag, CouplingManager)::PointSourceTraits::template PointSource<1>);
SET_TYPE_PROP(BloodTransportCCTypeTag, PointSourceHelper, typename GET_PROP_TYPE(TypeTag, CouplingManager)::PointSourceTraits::template PointSourceHelper<1>);

} // end namespace Properties
} // end namespace Dumux

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using BulkFlowTypeTag = TTAG(TissueCCTypeTag);
    using LowDimFlowTypeTag = TTAG(BloodFlowCCTypeTag);

    // start the timer
    Dune::Timer timer;

    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    GridManager<typename GET_PROP_TYPE(BulkFlowTypeTag, Grid)> bulkGridManager;
    bulkGridManager.init("Tissue"); // pass parameter group

    GridManager<typename GET_PROP_TYPE(LowDimFlowTypeTag, Grid)> lowDimGridManager;
    lowDimGridManager.init("Vessel"); // pass parameter group
    auto lowDimGridData = lowDimGridManager.getGridData();

    // we compute on the leaf grid view
    const auto& bulkGridView = bulkGridManager.grid().leafGridView();
    const auto& lowDimGridView = lowDimGridManager.grid().leafGridView();

    // refine grid elements large than a threshold
    {
        auto& lowDimGrid = lowDimGridManager.grid();
        // make sure both grid have roughly the same element sizes (h_max)
        const auto length = (getParam<Dune::FieldVector<double, 3>>("Tissue.Grid.UpperRight")
                             -getParam<Dune::FieldVector<double, 3>>("Tissue.Grid.LowerLeft"));
        const auto cells = getParam<Dune::FieldVector<double, 3>>("Tissue.Grid.Cells");
        const auto refinement = double(1<<getParam<int>("Tissue.Grid.Refinement"));
        const auto maxElementLength = std::max({length[0]/(cells[0]*refinement), length[1]/(cells[1]*refinement), length[2]/(cells[2]*refinement)});
        std::cout << "Refining grid with " << lowDimGridView.size(0)
                  << " elements until max length: " << maxElementLength << std::endl;

        bool markedElements = true;
        while (markedElements)
        {
            markedElements = markElements(lowDimGrid, [maxElementLength](const auto& element)
            {
                return element.geometry().volume() > maxElementLength ? 1 : 0;
            }, false);

            if (markedElements)
            {
                lowDimGrid.preAdapt();
                lowDimGrid.adapt();
                lowDimGrid.postAdapt();
            }
        }

        auto maxLength = 0.0;
        auto minLength = 1.0;
        for (const auto& element : elements(lowDimGridView))
        {
            const auto length = element.geometry().volume();
            maxLength = std::max(maxLength, length);
            minLength = std::min(minLength, length);
        }

        std::cout << "After refinement: " << lowDimGridView.size(0) << " elements:" << std::endl
                  << "  -- max length: " << maxLength << std::endl
                  << "  -- min length: " << minLength << std::endl
                  << "  -- max level: " << lowDimGridView.grid().maxLevel() << std::endl;
    }

    // create the finite volume grid geometry
    using BulkFVGridGeometry = typename GET_PROP_TYPE(BulkFlowTypeTag, FVGridGeometry);
    auto bulkFvGridGeometry = std::make_shared<BulkFVGridGeometry>(bulkGridView);
    bulkFvGridGeometry->update();
    using LowDimFVGridGeometry = typename GET_PROP_TYPE(LowDimFlowTypeTag, FVGridGeometry);
    auto lowDimFvGridGeometry = std::make_shared<LowDimFVGridGeometry>(lowDimGridView);
    lowDimFvGridGeometry->update();

    const auto gridSetupTime = timer.elapsed();
    std::cout << "Setting up grid geometry took " << timer.elapsed() << " seconds." << std::endl;

    ////////////////////////////////////////////////////////////
    // If the flow was not solved yet solve it
    ////////////////////////////////////////////////////////////
    const bool enableVtkOutput = getParam<bool>("Vtk.EnableOutput", true);
    const bool solveFlow = getParam<bool>("Problem.SolveFlow", true);
    const std::string flowOutFileName = getParam<std::string>("Problem.FlowFileName");
    double flowTime = 0.0; // time measurement

    // the flow data for the transport problem
    std::vector<double> volumeFluxBulk, volumeFluxLowDim, sourceBulk, sourceLowDim;

    const auto refine = getParam<int>("Tissue.Grid.Refinement", 0);
    const auto cells = getParam<std::vector<int>>("Tissue.Grid.Cells")[0]*(1<<refine);

    if (solveFlow)
    {
        ////////////////////////////////////////////////////////////
        // Flow: prepare variables and problem
        ////////////////////////////////////////////////////////////

        // the mixed dimension type traits
        using FlowMDTraits = MultiDomainTraits<BulkFlowTypeTag, LowDimFlowTypeTag>;
        constexpr auto bulkIdx = FlowMDTraits::template SubDomain<0>::Index();
        constexpr auto lowDimIdx = FlowMDTraits::template SubDomain<1>::Index();

        // the coupling manager
        using CouplingManager = typename GET_PROP_TYPE(BulkFlowTypeTag, CouplingManager);
        auto couplingManager = std::make_shared<CouplingManager>(bulkFvGridGeometry, lowDimFvGridGeometry);

        // the problem (initial and boundary conditions)
        using BulkFlowProblem = typename GET_PROP_TYPE(BulkFlowTypeTag, Problem);
        auto bulkFlowProblem = std::make_shared<BulkFlowProblem>(bulkFvGridGeometry, couplingManager, "TissueFlow");
        using LowDimFlowProblem = typename GET_PROP_TYPE(LowDimFlowTypeTag, Problem);
        auto lowDimFlowProblem = std::make_shared<LowDimFlowProblem>(lowDimFvGridGeometry, couplingManager, lowDimGridData, "BloodFlow");

        // the solution vector
        FlowMDTraits::SolutionVector sol;
        sol[bulkIdx].resize(bulkFvGridGeometry->numDofs());
        sol[lowDimIdx].resize(lowDimFvGridGeometry->numDofs());
        sol = 0.0;

        // initial solution
        bulkFlowProblem->applyInitialSolution(sol[bulkIdx]);
        lowDimFlowProblem->applyInitialSolution(sol[lowDimIdx]);

        couplingManager->init(bulkFlowProblem, lowDimFlowProblem, sol);
        bulkFlowProblem->computePointSourceMap();
        lowDimFlowProblem->computePointSourceMap();

        // the grid variables
        using BulkGridVariables = typename GET_PROP_TYPE(BulkFlowTypeTag, GridVariables);
        auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkFlowProblem, bulkFvGridGeometry);
        bulkGridVariables->init(sol[bulkIdx]);
        using LowDimGridVariables = typename GET_PROP_TYPE(LowDimFlowTypeTag, GridVariables);
        auto lowDimGridVariables = std::make_shared<LowDimGridVariables>(lowDimFlowProblem, lowDimFvGridGeometry);
        lowDimGridVariables->init(sol[lowDimIdx]);

        // intialize the vtk output module
        using BulkSolution = std::decay_t<decltype(sol[bulkIdx])>;
        VtkOutputModule<BulkGridVariables, BulkSolution> bulkVtkWriter(*bulkGridVariables, sol[bulkIdx], bulkFlowProblem->name());
        GET_PROP_TYPE(BulkFlowTypeTag, VtkOutputFields)::initOutputModule(bulkVtkWriter);
        if (enableVtkOutput) bulkVtkWriter.write(0.0);

        using LowDimSolution = std::decay_t<decltype(sol[lowDimIdx])>;
        VtkOutputModule<LowDimGridVariables, LowDimSolution> lowDimVtkWriter(*lowDimGridVariables, sol[lowDimIdx], lowDimFlowProblem->name());
        GET_PROP_TYPE(LowDimFlowTypeTag, VtkOutputFields)::initOutputModule(lowDimVtkWriter);
        lowDimFlowProblem->addVtkOutputFields(lowDimVtkWriter);
        if (enableVtkOutput) lowDimVtkWriter.write(0.0);

        // the assembler with time loop for instationary problem
        using Assembler = MultiDomainFVAssembler<FlowMDTraits, CouplingManager, DiffMethod::numeric>;
        auto assembler = std::make_shared<Assembler>(std::make_tuple(bulkFlowProblem, lowDimFlowProblem),
                                                     std::make_tuple(bulkFvGridGeometry, lowDimFvGridGeometry),
                                                     std::make_tuple(bulkGridVariables, lowDimGridVariables),
                                                     couplingManager);

        // the linear solver
        using LinearSolver = BlockDiagILU0BiCGSTABSolver;
        auto linearSolver = std::make_shared<LinearSolver>();

        // the non-linear solver
        using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
        NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

        nonLinearSolver.solve(sol);

        ////////////////////////////////////////////////////////////
        // solve for the stationary velocity field
        ////////////////////////////////////////////////////////////

        // Dune::Timer assembleTimer(false), solveTimer(false), updateTimer(false);
        // std::cout << "Assembling and solving linear system ("
        //           << linearSolver->name() << ") ... " << std::flush;
        //
        // // assemble stiffness matrix
        // assembleTimer.start();
        // couplingManager->updateSolution(sol);
        // assembler->assembleJacobianAndResidual(sol);
        // assembleTimer.stop();
        //
        // // we solve Jx = -r to save the vector update
        // auto& b = assembler->residual();
        // b *= -1.0;
        //
        // // solve linear system
        // solveTimer.start();
        // const bool converged = linearSolver->template solve<2>(assembler->jacobian(), sol, b);
        // if (!converged) DUNE_THROW(Dune::MathError, "Linear solver did not converge!");
        // solveTimer.stop();
        //
        // // update variables
        // updateTimer.start();
        // couplingManager->updateSolution(sol);
        // bulkGridVariables->update(sol[bulkIdx]);
        // lowDimGridVariables->update(sol[lowDimIdx]);
        // updateTimer.stop();
        //
        // std::cout << "done.\n";
        // const auto elapsedTot = assembleTimer.elapsed() + solveTimer.elapsed() + updateTimer.elapsed();
        // std::cout << "Assemble/solve/update time: "
        //           <<  assembleTimer.elapsed() << "(" << 100*assembleTimer.elapsed()/elapsedTot << "%)/"
        //           <<  solveTimer.elapsed() << "(" << 100*solveTimer.elapsed()/elapsedTot << "%)/"
        //           <<  updateTimer.elapsed() << "(" << 100*updateTimer.elapsed()/elapsedTot << "%)"
        //           << "\n";

        // output the source terms
        bulkFlowProblem->computeSourceIntegral(sol[bulkIdx], *bulkGridVariables);
        lowDimFlowProblem->computeSourceIntegral(sol[lowDimIdx], *lowDimGridVariables);

        // write vtk output
        if (enableVtkOutput)
        {
            bulkVtkWriter.write(1.0);
            lowDimVtkWriter.write(1.0);
        }

        ////////////////////////////////////////////////////////////
        // compute the velocities and sources
        ////////////////////////////////////////////////////////////

        volumeFluxBulk = bulkFlowProblem->computeVolumeFluxes(sol[bulkIdx], *bulkGridVariables);
        volumeFluxLowDim = lowDimFlowProblem->computeVolumeFluxes(sol[lowDimIdx], *lowDimGridVariables);
        sourceBulk = bulkFlowProblem->computeVolumeSources(sol[bulkIdx], *bulkGridVariables);
        sourceLowDim = lowDimFlowProblem->computeVolumeSources(sol[lowDimIdx], *lowDimGridVariables);

        writeContainerToFile(volumeFluxBulk, flowOutFileName + "_fluxbulk.out");
        writeContainerToFile(volumeFluxLowDim, flowOutFileName + "_fluxlowdim.out");
        writeContainerToFile(sourceBulk, flowOutFileName + "_sourcebulk.out");
        writeContainerToFile(sourceLowDim, flowOutFileName + "_sourcelowdim.out");

        flowTime = timer.elapsed()-gridSetupTime;
        std::cout << "Flow computation took " << timer.elapsed()-gridSetupTime << " seconds." << std::endl;

        // write out the pressure solution in index ordering
        std::vector<double> pSol(bulkFvGridGeometry->gridView().size(0), 0.0);
        for (const auto& element : elements(bulkFvGridGeometry->gridView()))
            pSol[bulkFvGridGeometry->elementMapper().index(element)] = sol[bulkIdx][bulkFvGridGeometry->elementMapper().index(element)];

        writeContainerToFile(pSol, "pressure-" + std::to_string(cells) + ".out");
    }

    // flow was already solved, read from output file
    else
    {
        volumeFluxBulk = readFileToContainer<std::vector<double>>(flowOutFileName + "_fluxbulk.out");
        volumeFluxLowDim = readFileToContainer<std::vector<double>>(flowOutFileName + "_fluxlowdim.out");
        sourceBulk = readFileToContainer<std::vector<double>>(flowOutFileName + "_sourcebulk.out");
        sourceLowDim = readFileToContainer<std::vector<double>>(flowOutFileName + "_sourcelowdim.out");

        flowTime = timer.elapsed();
        std::cout << "Reading flow data from file took " << timer.elapsed() << " seconds." << std::endl;
    }

    ////////////////////////////////////////////////////////////
    // the transport problem
    ////////////////////////////////////////////////////////////

    // Define the sub problem type tags
    using BulkTransportTypeTag = TTAG(TissueTransportCCTypeTag);
    using LowDimTransportTypeTag = TTAG(BloodTransportCCTypeTag);

    // we use the same grid as for flow
    // the mixed dimension type traits
    using TransportMDTraits = MultiDomainTraits<BulkTransportTypeTag, LowDimTransportTypeTag>;
    constexpr auto bulkIdx = TransportMDTraits::template SubDomain<0>::Index();
    constexpr auto lowDimIdx = TransportMDTraits::template SubDomain<1>::Index();

    // the coupling manager
    using CouplingManager = typename GET_PROP_TYPE(BulkTransportTypeTag, CouplingManager);
    auto couplingManager = std::make_shared<CouplingManager>(bulkFvGridGeometry, lowDimFvGridGeometry);

    // the problem (initial and boundary conditions)
    using BulkTransportProblem = typename GET_PROP_TYPE(BulkTransportTypeTag, Problem);
    auto bulkTransportProblem = std::make_shared<BulkTransportProblem>(bulkFvGridGeometry, couplingManager, "TissueTransport");
    bulkTransportProblem->spatialParams().setVolumeFlux(volumeFluxBulk);
    bulkTransportProblem->spatialParams().setVolumeSource(sourceBulk);
    using LowDimTransportProblem = typename GET_PROP_TYPE(LowDimTransportTypeTag, Problem);
    auto lowDimTransportProblem = std::make_shared<LowDimTransportProblem>(lowDimFvGridGeometry, couplingManager, lowDimGridData, "BloodTransport");
    lowDimTransportProblem->spatialParams().setVolumeFlux(volumeFluxLowDim);
    lowDimTransportProblem->spatialParams().setVolumeSource(sourceLowDim);

    // the solution vector
    TransportMDTraits::SolutionVector sol;
    sol[bulkIdx].resize(bulkFvGridGeometry->numDofs());
    sol[lowDimIdx].resize(lowDimFvGridGeometry->numDofs());
    bulkTransportProblem->applyInitialSolution(sol[bulkIdx]);
    lowDimTransportProblem->applyInitialSolution(sol[lowDimIdx]);
    auto oldSol = sol;

    couplingManager->init(bulkTransportProblem, lowDimTransportProblem, sol);
    bulkTransportProblem->computePointSourceMap();
    lowDimTransportProblem->computePointSourceMap();

    // the grid variables
    using BulkGridVariables = typename GET_PROP_TYPE(BulkTransportTypeTag, GridVariables);
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkTransportProblem, bulkFvGridGeometry);
    bulkGridVariables->init(sol[bulkIdx]);
    using LowDimGridVariables = typename GET_PROP_TYPE(LowDimTransportTypeTag, GridVariables);
    auto lowDimGridVariables = std::make_shared<LowDimGridVariables>(lowDimTransportProblem, lowDimFvGridGeometry);
    lowDimGridVariables->init(sol[lowDimIdx]);

    // intialize the vtk output module
    using BulkSolution = std::decay_t<decltype(sol[bulkIdx])>;
    VtkOutputModule<BulkGridVariables, BulkSolution> bulkVtkWriter(*bulkGridVariables, sol[bulkIdx], bulkTransportProblem->name());
    GET_PROP_TYPE(BulkTransportTypeTag, VtkOutputFields)::initOutputModule(bulkVtkWriter);
    bulkTransportProblem->addVtkOutputFields(bulkVtkWriter);
    if (enableVtkOutput) bulkVtkWriter.write(0.0);

    using LowDimSolution = std::decay_t<decltype(sol[lowDimIdx])>;
    VtkOutputModule<LowDimGridVariables, LowDimSolution> lowDimVtkWriter(*lowDimGridVariables, sol[lowDimIdx], lowDimTransportProblem->name());
    GET_PROP_TYPE(LowDimTransportTypeTag, VtkOutputFields)::initOutputModule(lowDimVtkWriter);
    lowDimTransportProblem->addVtkOutputFields(lowDimVtkWriter);
    if (enableVtkOutput) lowDimVtkWriter.write(0.0);

    // the mr signal provessor
    // MRSignalProcessor<TransportMDTraits> mrSignalProcessor(bulkTransportProblem, lowDimTransportProblem);

    //! get some time loop parameters
    const auto tEnd = getParam<double>("TimeLoop.TEnd");
    const auto timeBeforeInjection = getParam<double>("TimeLoop.TimeBeforeInjection");

    const auto dtFactor = getParam<double>("TimeLoop.DtFactor", 1.0); // a factor for time convergence tests
    const auto maxDt = getParam<double>("TimeLoop.MaxTimeStepSize")*dtFactor;
    const auto maxDtInjection = getParam<double>("TimeLoop.MaxDtInjection")*dtFactor;
    const auto initDtInjection = getParam<double>("TimeLoop.InitDtInjection")*dtFactor;

    const auto injectionTime = getParam<double>("TimeLoop.InjectionTime");
    const auto injectionEnd = timeBeforeInjection + injectionTime;
    const auto timeControlPower = getParam<double>("TimeLoop.PowerFunction");
    const auto timeControlPowerInj = getParam<double>("TimeLoop.PowerFunctionInjection");
    const auto timeStepFactorInjection = (initDtInjection - maxDtInjection)/(std::pow(timeBeforeInjection, timeControlPowerInj) - std::pow(injectionEnd, timeControlPowerInj));
    const auto timeStepOffsetInjection = initDtInjection - timeStepFactorInjection*std::pow(timeBeforeInjection, timeControlPowerInj);
    const auto timeStepFactor = (maxDtInjection - maxDt)/(std::pow(injectionEnd, timeControlPower) - std::pow(tEnd, timeControlPower));
    const auto timeStepOffset = maxDtInjection - timeStepFactor*std::pow(injectionEnd, timeControlPower);

    //! instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<double>>(0.0, timeBeforeInjection, tEnd);

    // the assembler with time loop for instationary problem
    using Assembler = MultiDomainFVAssembler<TransportMDTraits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(bulkTransportProblem, lowDimTransportProblem),
                                                 std::make_tuple(bulkFvGridGeometry, lowDimFvGridGeometry),
                                                 std::make_tuple(bulkGridVariables, lowDimGridVariables),
                                                 couplingManager,
                                                 timeLoop);

    // the linear solver
    using LinearSolver = BlockDiagILU0BiCGSTABSolver;
    auto linearSolver = std::make_shared<LinearSolver>();

    ////////////////////////////////////////////////////////////
    // solve for the instationary contrast agent field
    ////////////////////////////////////////////////////////////
    std::vector<double> xSol(bulkFvGridGeometry->gridView().size(0), 0.0);

    //! start the time loop
    timeLoop->start(); do
    {
        // set previous solution for storage evaluations
        assembler->setPreviousSolution(oldSol);

        Dune::Timer assembleTimer(false), solveTimer(false), updateTimer(false);
        std::cout << "\nAssembling and solving linear system ("
                  << linearSolver->name() << ") ... " << std::flush;

        // assemble stiffness matrix
        assembleTimer.start();
        lowDimTransportProblem->setTime(timeLoop->time() + timeLoop->timeStepSize());
        couplingManager->updateSolution(sol);
        assembler->assembleJacobianAndResidual(sol);
        assembleTimer.stop();

        // solve linear system
        solveTimer.start();
        auto deltaSol = sol;
        const bool converged = linearSolver->template solve<2>(assembler->jacobian(), deltaSol, assembler->residual());
        if (!converged) DUNE_THROW(Dune::MathError, "Linear solver did not converge!");
        solveTimer.stop();

        // update variables
        updateTimer.start();
        sol -= deltaSol;
        couplingManager->updateSolution(sol);
        bulkGridVariables->update(sol[bulkIdx]);
        lowDimGridVariables->update(sol[lowDimIdx]);
        updateTimer.stop();

        std::cout << "done.\n";
        const auto elapsedTot = assembleTimer.elapsed() + solveTimer.elapsed() + updateTimer.elapsed();
        std::cout << "Assemble/solve/update time: "
                  <<  assembleTimer.elapsed() << "(" << 100*assembleTimer.elapsed()/elapsedTot << "%)/"
                  <<  solveTimer.elapsed() << "(" << 100*solveTimer.elapsed()/elapsedTot << "%)/"
                  <<  updateTimer.elapsed() << "(" << 100*updateTimer.elapsed()/elapsedTot << "%)"
                  << "\n";

        // make the new solution the old solution
        oldSol = sol;
        bulkGridVariables->advanceTimeStep();
        lowDimGridVariables->advanceTimeStep();

        // output the source terms
        bulkTransportProblem->computeSourceIntegral(sol[bulkIdx], *bulkGridVariables);
        lowDimTransportProblem->computeSourceIntegral(sol[lowDimIdx], *lowDimGridVariables);

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // write vtk output
        if (enableVtkOutput)
        {
            bulkVtkWriter.write(timeLoop->time());
            lowDimVtkWriter.write(timeLoop->time());
        }

        // write out the pressure solution in index ordering
        for (const auto& element : elements(bulkFvGridGeometry->gridView()))
            xSol[bulkFvGridGeometry->elementMapper().index(element)] = sol[bulkIdx][bulkFvGridGeometry->elementMapper().index(element)];

        writeContainerToFile(xSol, "x-" + std::to_string(cells) + "-" + std::to_string(timeLoop->timeStepIndex()) + ".out");

        // generate the MR signal
        // mrSignalProcessor.generateSignal(timeLoop->time(), sol, *bulkGridVariables, *lowDimGridVariables, *couplingManager);

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // the first time step is just the time before injection where nothing happens
        // then we start injection
        if (timeLoop->time() < injectionEnd)
            timeLoop->setTimeStepSize(std::pow(timeLoop->time(), timeControlPowerInj)*timeStepFactorInjection + timeStepOffsetInjection);
        else
            timeLoop->setTimeStepSize(std::pow(timeLoop->time(), timeControlPower)*timeStepFactor + timeStepOffset);

    } while (!timeLoop->finished());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // mrSignalProcessor.finalize();
    timeLoop->finalize();

    std::cout << "Transport computation took " << timer.elapsed() - flowTime - gridSetupTime << " seconds." << std::endl;
    std::cout << "Simulation took " << timer.elapsed() << " seconds." << std::endl;

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
}
/////////////////////////////////////////////
///////////// Error handler /////////////////
/////////////////////////////////////////////
catch (const Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (const Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (const Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
