#!/usr/bin/env python3
import matplotlib.pyplot as plt
from numpy import genfromtxt
import numpy as np

import sys
import os
# because when executing this script as a symlink in build-cmake it won't find the library
sys.path.insert(0, os.path.abspath('.'))

import pymsbern

# run simulation
params = {}
input_file = "data/msbern.input"
sim = np.array(pymsbern.run_forward_model(params=params, input=input_file, verbose=True))

font = {'family' : 'normal', 'weight' : 'bold', 'size'   : 20}
font2 = {'family' : 'normal', 'weight' : 'normal', 'size'   : 20}
plt.rc('font', **font2)
fig = plt.figure(dpi=72, figsize=(8, 5))

plt.ylabel('NMR signal \n (normalized to baseline)', fontsize=font['size'], fontweight=font['weight'])
plt.xlabel('time in s', fontsize=font['size'], fontweight=font['weight'])
t = [i*1.4 for i in range(0,80)]
plt.plot(t, sim, "-", linewidth=4.0, label="simulation")
ax = plt.gca()
ax.tick_params(axis='both', which='major', labelsize=16)
ax.tick_params(axis='both', which='minor', labelsize=16)

plt.xlim([0, 120])
plt.legend(loc='upper left')
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0, rect=[0.05, 0, 1, 1])
# fig.savefig("signal.pdf")
# fig.savefig("signal.png")
plt.show()
