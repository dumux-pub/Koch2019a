#!/usr/bin/env python

from __future__ import division, print_function

import os
import sys
import matplotlib.pyplot as plt
from matplotlib import rcParams
import numpy as np
import scipy.stats as st
import json
import subprocess
import errno
import copy
import multiprocessing as mp
import corner
import argparse

# emcee library for MCMC
import emcee
print("Running with emcee version {}".format(emcee.__version__))

# add location where to find the forward model wrapper
sys.path.append(os.path.abspath("../"))
import pymsbern


# the forward model
def run_model(random_params):
    paramnames = sorted(["MRSignal.T1TissuePreContrast", "InflowCurve.a", "InflowCurve.b", "InflowCurve.tp",
                         "VesselWall.DiffusionCoefficient", "MRSignal.KappaB", "MRSignal.KappaT"])

    # the parameters passed to the model
    params = {}

    # convert to dictionary again
    for i, name in enumerate(paramnames):
        params[name] = str(random_params[i])

    # create output parameters and dump parameters
    params["Problem.LesionIntensity"] = "1" # healthy: 0, sick: 1
    params["Problem.SolveFlow"] = "false" # only transport
    params["VesselWall.FiltrationCoefficient"] = "9.746174233413226E-011" # Lp
    params["MRSignal.T2TissuePreContrast"] = "8.000000000000000E-002" # seconds
    # convert back from log scale
    params["VesselWall.DiffusionCoefficient"] = str(10**(-float(params["VesselWall.DiffusionCoefficient"])))

    params["Vessel.Grid.File"] = "../data/ratbrain.dgf"

    verbose = False
    data = np.array(pymsbern.run_forward_model(params, "../data/msbern.input", verbose))

    return data


# compute the log-likelihood p(y|theta) as gaussian likelihood as function of model error
def log_likelihood(data, observation, stddev=0.002):
    return -0.5*np.dot(data-observation, data-observation)/stddev


# the prior, we use a uniform distribution within the physically acceptable range
def log_prior(params):
    paramnames = sorted(["MRSignal.T1TissuePreContrast", "InflowCurve.a", "InflowCurve.b", "InflowCurve.tp",
                         "VesselWall.DiffusionCoefficient", "MRSignal.KappaB", "MRSignal.KappaT"])

    if not (0.0 < params[paramnames.index("MRSignal.KappaB")] < 100.0):
        print("-- MRSignal.KappaB out of range: {}".format(params[paramnames.index("MRSignal.KappaB")]))
        return -np.inf

    if not (0.0 < params[paramnames.index("MRSignal.KappaT")] < 100.0):
        print("-- MRSignal.KappaT out of range: {}".format(params[paramnames.index("MRSignal.KappaT")]))
        return -np.inf

    if not (2.0 < params[paramnames.index("InflowCurve.tp")] < 15.0):
        print("-- InflowCurve.tp out of range: {}".format(params[paramnames.index("InflowCurve.tp")]))
        return -np.inf

    if not (0.0 < params[paramnames.index("InflowCurve.b")] < 2.0):
        print("-- InflowCurve.b out of range: {}".format(params[paramnames.index("InflowCurve.b")]))
        return -np.inf

    if not (0.0 < params[paramnames.index("InflowCurve.a")] < 100.0):
        print("-- InflowCurve.a out of range: {}".format(params[paramnames.index("InflowCurve.a")]))
        return -np.inf

    if not (0.7 < params[paramnames.index("MRSignal.T1TissuePreContrast")] < 3.0):
        print("-- MRSignal.T1TissuePreContrast out of range: {}".format(params[paramnames.index("MRSignal.T1TissuePreContrast")]))
        return -np.inf

    if not (10.0 < params[paramnames.index("VesselWall.DiffusionCoefficient")] < 18.0):
        print("-- VesselWall.DiffusionCoefficient out of range: {}".format(params[paramnames.index("VesselWall.DiffusionCoefficient")]))
        return -np.inf

    else:
        return 0.0


# the posterior p(theta | y) = p(y | theta)*p(theta) (Bayesian update) (without the normalization constant)
def log_posterior(params, observation):
    logprior = log_prior(params)
    if np.isinf(logprior): # if the parameters have zero probability we can skip the model run
        return logprior
    else:
        data = run_model(params)
        return log_likelihood(data, observation) + logprior


# main routine
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='MCMC for the MS Bern problem')
    parser.add_argument('--obs', default='msbern-sick.obs', help='the observation file')
    parser.add_argument('--out', default='msbern-emcee.h5', help='the result output file')
    args = vars(parser.parse_args())

    # get the observation we want to reproduce
    observation = np.genfromtxt(args["obs"])
    print("Read observations from {}".format(args["obs"]))

    with mp.Pool() as pool:

        # choose an initial parameter set
        initial = {}
        initial["MRSignal.T1TissuePreContrast"] = 1.4 # seconds
        initial["InflowCurve.a"] = 3.812619924254471E+001 # scaling params mol*s/m3
        initial["InflowCurve.b"] = 1.934938693976044E+000 # baseline mol/m3
        initial["InflowCurve.tp"] = 5.000000000000000E+000 # time to peak in s
        initial["VesselWall.DiffusionCoefficient"] = 14.0 # D wall
        initial["MRSignal.KappaB"] = 1.502480256948131E+001
        initial["MRSignal.KappaT"] = 0.1

        # convert to plain array for emcee
        theta = np.array([v for k,v in sorted(initial.items())])

        # set the number of walkers and the parameter space dimension
        ndim, nwalkers = len(theta), 100

        # Set up the storage backend
        # Don't forget to clear it in case the file already exists
        backend = emcee.backends.HDFBackend(args["out"])
        backend.reset(nwalkers, ndim)

        # setup the sampler
        sampler = emcee.EnsembleSampler(nwalkers, ndim, log_posterior, args=(observation,), pool=pool, backend=backend)

        # set the maximum number of iterations
        nmax = 1000000

        # the initial position of the walkers
        pos0 = [theta + 1e-1*np.multiply(np.random.randn(ndim), theta) for i in range(nwalkers)]

        # We'll track how the average autocorrelation time estimate changes
        autocorrIdx = 0
        autocorr = np.empty(nmax)
        tauold = np.inf
        autocorreverynsteps = 10

        # sample step by step using the generator sampler.sample
        for sample in sampler.sample(pos0, iterations=nmax, progress=False):

            # always print the current mean acceptance fraction
            print("Current mean acceptance fraction: {}".format(np.mean(sampler.acceptance_fraction)))

            # only check convergence every 10 steps (every 1000 samples)
            if sampler.iteration % autocorreverynsteps:
                continue

            # compute the autocorrelation time so far
            # using tol=0 means that we'll always get an estimate even if it isn't trustworthy
            tau = sampler.get_autocorr_time(tol=0)
            autocorr[autocorrIdx] = np.mean(tau) # average over walkers
            autocorrIdx += 1

            # output current autocorrelation estimate
            print("Current mean autocorrelation time estimate: {}".format(np.mean(tau)))

            # check convergence
            converged = np.all(tau * 100 < sampler.iteration)
            converged &= np.all(np.abs(tauold - tau) / tau < 0.01)
            if converged:
                break
            tauold = tau

        print("Finished MCMC, plotting autocorrelation time estimate...")
        # plot development of autocorrelation estimate
        steps = autocorreverynsteps*np.arange(1, autocorrIdx+1)
        taus = autocorr[:autocorrIdx]
        plt.plot(steps, steps / float(autocorreverynsteps), "--k")
        plt.plot(steps, taus)
        plt.xlim(0, steps.max())
        plt.ylim(0, taus.max() + 0.1*(taus.max() - taus.min()))
        plt.xlabel("number of steps")
        plt.ylabel(r"mean $\hat{\tau}$");
        fig.savefig("autocorrelation_time.pdf")
        plt.show()
