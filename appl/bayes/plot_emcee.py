#!/usr/bin/env python

from __future__ import division, print_function

import os
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import corner
import argparse

# emcee library for MCMC
import emcee
print("Running with emcee version {}".format(emcee.__version__))

# command line argument
if len(sys.argv) != 2:
    raise IOError('You have to provide a HDF5 file of the emcee output as only argument!')
filename = str(sys.argv[1])

# read the file
reader = emcee.backends.HDFBackend(filename)
try:
    tau = reader.get_autocorr_time(tol=0)
except emcee.autocorr.AutocorrError:
    tau = 2
burnin = int(2*np.max(tau))
thin = int(0.5*np.min(tau))
samples = reader.get_chain(discard=burnin, flat=True, thin=thin)
log_prob_samples = reader.get_log_prob(discard=burnin, flat=True, thin=thin)

print("mean auto-correlation time: {0}".format(np.mean(tau)))
print("thin: {0}".format(thin))
print("burn-in: {0}".format(burnin))
print("flat chain shape: {0}".format(samples.shape))
print("Mean acceptance fraction: {}".format(np.mean(reader.accepted/reader.iteration)))
print("flat log prob shape: {0}".format(log_prob_samples.shape))
# do corner plot of the results
labels = {}
labels["MRSignal.T1TissuePreContrast"] = r"$T_1^{pre}$"
labels["InflowCurve.a"] = r"$a$"
labels["InflowCurve.b"] = r"$b$"
labels["InflowCurve.tp"] = r"$t_p$"
labels["VesselWall.DiffusionCoefficient"] = r"$\log D_e$"
labels["MRSignal.KappaB"] = r"$\kappa_B$"
labels["MRSignal.KappaT"] = r"$\kappa_T$"

plt.style.use('ggplot')
fig = corner.corner(samples,
                    labels=[v for k,v in sorted(labels.items())],
                    show_titles=True,
                    quantiles=[0.05, 0.95],
                    #truths=best_fit_list,
                    plot_contours=False, # hist2d_kwargs
                    plot_density=False,
                    no_fill_contours=True)
fig.savefig("corner_emcee.pdf")
