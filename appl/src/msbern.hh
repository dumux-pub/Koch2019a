// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_MSBERN_HH
#define DUMUX_MSBERN_HH

#include <iostream>
#include <unordered_map>
#include <algorithm>

#include <dune/common/timer.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/container.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/adaptive/markelements.hh>
#include <dumux/porousmediumflow/velocityoutput.hh>

#include <dumux/multidomain/traits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include "properties.hh"
#include "mrsignal.hh"

namespace Dumux {

// main driver function
std::vector<double>
runForwardModel(const std::unordered_map<std::string, std::string>& params = std::unordered_map<std::string, std::string>{},
                const std::string& parameterFileName = "",
                bool verbose = true)
{
    auto setParams = [&](Dune::ParameterTree& tree){
        for (const auto& p : params)
            tree[p.first] = p.second;
    };

    if (parameterFileName != "")
        Parameters::init(parameterFileName, setParams, /*inputFileHasPrecedence*/false);
    else
        Parameters::init(setParams);

    std::streambuf* coutBuffer = std::cout.rdbuf();
    std::streambuf* cerrBuffer = std::cerr.rdbuf();
    std::ofstream fout("/dev/null");

    if (!verbose)
    {
        // redirect output
        std::cout.rdbuf(fout.rdbuf());
        std::cerr.rdbuf(fout.rdbuf());
    }

    // Define the sub problem type tags
    using BulkFlowTypeTag = Properties::TTag::Tissue;
    using LowDimFlowTypeTag = Properties::TTag::BloodFlow;

    // start the timer
    Dune::Timer timer;

    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    GridManager<GetPropType<BulkFlowTypeTag, Properties::Grid>> bulkGridManager;
    bulkGridManager.init("Tissue"); // pass parameter group

    GridManager<GetPropType<LowDimFlowTypeTag, Properties::Grid>> lowDimGridManager;
    lowDimGridManager.init("Vessel"); // pass parameter group
    auto lowDimGridData = lowDimGridManager.getGridData();

    // we compute on the leaf grid view
    const auto& bulkGridView = bulkGridManager.grid().leafGridView();
    const auto& lowDimGridView = lowDimGridManager.grid().leafGridView();

    // refine grid elements large than a threshold
    {
        auto& lowDimGrid = lowDimGridManager.grid();
        // make sure both grid have roughly the same element sizes (h_max)
        const auto length = (getParam<Dune::FieldVector<double, 3>>("Tissue.Grid.UpperRight")
                             -getParam<Dune::FieldVector<double, 3>>("Tissue.Grid.LowerLeft"));
        const auto cells = getParam<Dune::FieldVector<double, 3>>("Tissue.Grid.Cells");
        const auto factor = getParam<double>("Vessel.Grid.MaxElementLengthFactor", 1.0);
        const auto maxElementLength = std::max({length[0]/cells[0], length[1]/cells[1], length[2]/cells[2]})*factor;
        std::cout << "Refining grid with " << lowDimGridView.size(0)
                  << " elements until max length: " << maxElementLength << std::endl;

        bool markedElements = true;
        while (markedElements)
        {
            markedElements = markElements(lowDimGrid, [maxElementLength](const auto& element)
            {
                return element.geometry().volume() > maxElementLength ? 1 : 0;
            }, false);

            if (markedElements)
            {
                lowDimGrid.preAdapt();
                lowDimGrid.adapt();
                lowDimGrid.postAdapt();
            }
        }

        auto maxLength = 0.0;
        auto minLength = 1.0;
        for (const auto& element : elements(lowDimGridView))
        {
            const auto length = element.geometry().volume();
            maxLength = std::max(maxLength, length);
            minLength = std::min(minLength, length);
        }

        std::cout << "After refinement: " << lowDimGridView.size(0) << " elements:" << std::endl
                  << "  -- max length: " << maxLength << std::endl
                  << "  -- min length: " << minLength << std::endl
                  << "  -- max level: " << lowDimGridView.grid().maxLevel() << std::endl;
    }

    // create the finite volume grid geometry
    using BulkGridGeometry = GetPropType<BulkFlowTypeTag, Properties::GridGeometry>;
    auto bulkGridGeometry = std::make_shared<BulkGridGeometry>(bulkGridView);
    using LowDimGridGeometry = GetPropType<LowDimFlowTypeTag, Properties::GridGeometry>;
    auto lowDimGridGeometry = std::make_shared<LowDimGridGeometry>(lowDimGridView);

    const auto gridSetupTime = timer.elapsed();
    std::cout << "Setting up grid geometry took " << timer.elapsed() << " seconds." << std::endl;

    ////////////////////////////////////////////////////////////
    // If the flow was not solved yet solve it
    ////////////////////////////////////////////////////////////
    const bool enableVtkOutput = getParam<bool>("Vtk.EnableOutput", true);
    const bool solveFlow = getParam<bool>("Problem.SolveFlow", true);
    const std::string flowOutFileName = getParam<std::string>("Problem.FlowFileName");
    double flowTime = 0.0; // time measurement

    // the flow data for the transport problem
    std::vector<double> volumeFluxBulk, volumeFluxLowDim, sourceBulk, sourceLowDim;

    if (solveFlow)
    {
        ////////////////////////////////////////////////////////////
        // Flow: prepare variables and problem
        ////////////////////////////////////////////////////////////

        // the mixed dimension type traits
        using FlowMDTraits = MultiDomainTraits<BulkFlowTypeTag, LowDimFlowTypeTag>;
        constexpr auto bulkIdx = FlowMDTraits::template SubDomain<0>::Index();
        constexpr auto lowDimIdx = FlowMDTraits::template SubDomain<1>::Index();

        // the coupling manager
        using CouplingManager = GetPropType<BulkFlowTypeTag, Properties::CouplingManager>;
        auto couplingManager = std::make_shared<CouplingManager>(bulkGridGeometry, lowDimGridGeometry);

        // the problem (initial and boundary conditions)
        using BulkFlowProblem = GetPropType<BulkFlowTypeTag, Properties::Problem>;
        auto bulkFlowProblem = std::make_shared<BulkFlowProblem>(bulkGridGeometry, couplingManager, "TissueFlow");
        using LowDimFlowProblem = GetPropType<LowDimFlowTypeTag, Properties::Problem>;
        auto lowDimFlowProblem = std::make_shared<LowDimFlowProblem>(lowDimGridGeometry, couplingManager, lowDimGridData, "BloodFlow");

        // the solution vector
        FlowMDTraits::SolutionVector sol;

        // initial solution
        bulkFlowProblem->applyInitialSolution(sol[bulkIdx]);
        lowDimFlowProblem->applyInitialSolution(sol[lowDimIdx]);

        couplingManager->init(bulkFlowProblem, lowDimFlowProblem, sol);
        bulkFlowProblem->computePointSourceMap();
        lowDimFlowProblem->computePointSourceMap();

        // the grid variables
        using BulkGridVariables = GetPropType<BulkFlowTypeTag, Properties::GridVariables>;
        auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkFlowProblem, bulkGridGeometry);
        bulkGridVariables->init(sol[bulkIdx]);
        using LowDimGridVariables = GetPropType<LowDimFlowTypeTag, Properties::GridVariables>;
        auto lowDimGridVariables = std::make_shared<LowDimGridVariables>(lowDimFlowProblem, lowDimGridGeometry);
        lowDimGridVariables->init(sol[lowDimIdx]);

        // intialize the vtk output module
        using BulkSolution = std::decay_t<decltype(sol[bulkIdx])>;
        VtkOutputModule<BulkGridVariables, BulkSolution> bulkVtkWriter(*bulkGridVariables, sol[bulkIdx], bulkFlowProblem->name());
        GetPropType<BulkFlowTypeTag, Properties::IOFields>::initOutputModule(bulkVtkWriter);
        if (enableVtkOutput) bulkVtkWriter.write(0.0);

        using LowDimSolution = std::decay_t<decltype(sol[lowDimIdx])>;
        VtkOutputModule<LowDimGridVariables, LowDimSolution> lowDimVtkWriter(*lowDimGridVariables, sol[lowDimIdx], lowDimFlowProblem->name());
        GetPropType<LowDimFlowTypeTag, Properties::IOFields>::initOutputModule(lowDimVtkWriter);
        lowDimVtkWriter.addVelocityOutput(std::make_shared<PorousMediumFlowVelocityOutput<LowDimGridVariables,
                                                                      GetPropType<LowDimFlowTypeTag, Properties::FluxVariables>>>(*lowDimGridVariables));
        lowDimVtkWriter.addField(lowDimFlowProblem->spatialParams().getRadii(), "radius");
        if (enableVtkOutput) lowDimVtkWriter.write(0.0);

        // the assembler with time loop for instationary problem
        using Assembler = MultiDomainFVAssembler<FlowMDTraits, CouplingManager, DiffMethod::numeric>;
        auto assembler = std::make_shared<Assembler>(std::make_tuple(bulkFlowProblem, lowDimFlowProblem),
                                                     std::make_tuple(bulkGridGeometry, lowDimGridGeometry),
                                                     std::make_tuple(bulkGridVariables, lowDimGridVariables),
                                                     couplingManager);

        // the linear solver
        using LinearSolver = BlockDiagILU0BiCGSTABSolver;
        auto linearSolver = std::make_shared<LinearSolver>();

        ////////////////////////////////////////////////////////////
        // solve for the stationary velocity field
        ////////////////////////////////////////////////////////////
        // the non-linear solver
        using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
        NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);
        nonLinearSolver.solve(sol);

        // output the source terms
        // bulkFlowProblem->computeSourceIntegral(sol[bulkIdx], *bulkGridVariables);
        // lowDimFlowProblem->computeSourceIntegral(sol[lowDimIdx], *lowDimGridVariables);

        // write vtk output
        if (enableVtkOutput)
        {
            bulkVtkWriter.write(1.0);
            lowDimVtkWriter.write(1.0);
        }

        ////////////////////////////////////////////////////////////
        // compute the velocities and sources
        ////////////////////////////////////////////////////////////

        volumeFluxBulk = bulkFlowProblem->computeVolumeFluxes(sol[bulkIdx], *bulkGridVariables);
        volumeFluxLowDim = lowDimFlowProblem->computeVolumeFluxes(sol[lowDimIdx], *lowDimGridVariables);
        sourceBulk = bulkFlowProblem->computeVolumeSources(sol[bulkIdx], *bulkGridVariables);
        sourceLowDim = lowDimFlowProblem->computeVolumeSources(sol[lowDimIdx], *lowDimGridVariables);

        writeContainerToFile(volumeFluxBulk, flowOutFileName + "_fluxbulk.out");
        writeContainerToFile(volumeFluxLowDim, flowOutFileName + "_fluxlowdim.out");
        writeContainerToFile(sourceBulk, flowOutFileName + "_sourcebulk.out");
        writeContainerToFile(sourceLowDim, flowOutFileName + "_sourcelowdim.out");

        flowTime = timer.elapsed()-gridSetupTime;
        std::cout << "Flow computation took " << timer.elapsed()-gridSetupTime << " seconds." << std::endl;

    }

    // flow was already solved, read from output file
    else
    {
        volumeFluxBulk = readFileToContainer<std::vector<double>>(flowOutFileName + "_fluxbulk.out");
        volumeFluxLowDim = readFileToContainer<std::vector<double>>(flowOutFileName + "_fluxlowdim.out");
        sourceBulk = readFileToContainer<std::vector<double>>(flowOutFileName + "_sourcebulk.out");
        sourceLowDim = readFileToContainer<std::vector<double>>(flowOutFileName + "_sourcelowdim.out");

        flowTime = timer.elapsed();
        std::cout << "Reading flow data from file took " << timer.elapsed() << " seconds." << std::endl;
    }

    ////////////////////////////////////////////////////////////
    // the transport problem
    ////////////////////////////////////////////////////////////

    // Define the sub problem type tags
    using BulkTransportTypeTag = Properties::TTag::TissueTransport;
    using LowDimTransportTypeTag = Properties::TTag::BloodTransport;

    // we use the same grid as for flow
    // the mixed dimension type traits
    using TransportMDTraits = MultiDomainTraits<BulkTransportTypeTag, LowDimTransportTypeTag>;
    constexpr auto bulkIdx = TransportMDTraits::template SubDomain<0>::Index();
    constexpr auto lowDimIdx = TransportMDTraits::template SubDomain<1>::Index();

    // the coupling manager
    using CouplingManager = GetPropType<BulkTransportTypeTag, Properties::CouplingManager>;
    auto couplingManager = std::make_shared<CouplingManager>(bulkGridGeometry, lowDimGridGeometry);

    // the problem (initial and boundary conditions)
    using BulkTransportProblem = GetPropType<BulkTransportTypeTag, Properties::Problem>;
    auto bulkTransportProblem = std::make_shared<BulkTransportProblem>(bulkGridGeometry, couplingManager, "TissueTransport");
    bulkTransportProblem->spatialParams().setVolumeFlux(volumeFluxBulk);
    bulkTransportProblem->spatialParams().setVolumeSource(sourceBulk);
    using LowDimTransportProblem = GetPropType<LowDimTransportTypeTag, Properties::Problem>;
    auto lowDimTransportProblem = std::make_shared<LowDimTransportProblem>(lowDimGridGeometry, couplingManager, lowDimGridData, "BloodTransport");
    lowDimTransportProblem->spatialParams().setVolumeFlux(volumeFluxLowDim);
    lowDimTransportProblem->spatialParams().setVolumeSource(sourceLowDim);

    // the solution vector
    TransportMDTraits::SolutionVector sol;
    bulkTransportProblem->applyInitialSolution(sol[bulkIdx]);
    lowDimTransportProblem->applyInitialSolution(sol[lowDimIdx]);
    auto oldSol = sol;

    couplingManager->init(bulkTransportProblem, lowDimTransportProblem, sol);
    bulkTransportProblem->computePointSourceMap();
    lowDimTransportProblem->computePointSourceMap();

    // the grid variables
    using BulkGridVariables = GetPropType<BulkTransportTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkTransportProblem, bulkGridGeometry);
    bulkGridVariables->init(sol[bulkIdx]);
    using LowDimGridVariables = GetPropType<LowDimTransportTypeTag, Properties::GridVariables>;
    auto lowDimGridVariables = std::make_shared<LowDimGridVariables>(lowDimTransportProblem, lowDimGridGeometry);
    lowDimGridVariables->init(sol[lowDimIdx]);

    // intialize the vtk output module
    using BulkSolution = std::decay_t<decltype(sol[bulkIdx])>;
    VtkOutputModule<BulkGridVariables, BulkSolution> bulkVtkWriter(*bulkGridVariables, sol[bulkIdx], bulkTransportProblem->name());
    GetPropType<BulkTransportTypeTag, Properties::IOFields>::initOutputModule(bulkVtkWriter);
    bulkVtkWriter.addVolumeVariable( [](const auto& v){ return v.molarDensity()*v.moleFraction(0,0); }, "c (mmol/l)");
    if (enableVtkOutput) bulkVtkWriter.write(0.0);

    using LowDimSolution = std::decay_t<decltype(sol[lowDimIdx])>;
    VtkOutputModule<LowDimGridVariables, LowDimSolution> lowDimVtkWriter(*lowDimGridVariables, sol[lowDimIdx], lowDimTransportProblem->name());
    GetPropType<LowDimTransportTypeTag, Properties::IOFields>::initOutputModule(lowDimVtkWriter);
    lowDimVtkWriter.addField(lowDimTransportProblem->spatialParams().getRadii(), "radius");
    lowDimVtkWriter.addVolumeVariable( [](const auto& v){ return v.molarDensity()*v.moleFraction(0,0); }, "c (mmol/l)");
    if (enableVtkOutput) lowDimVtkWriter.write(0.0);

    // the mr signal provessor
    MRSignalProcessor<TransportMDTraits> mrSignalProcessor(bulkTransportProblem, lowDimTransportProblem, lowDimGridData);

    //! get some time loop parameters
    const auto tEnd = getParam<double>("TimeLoop.TEnd");
    const auto timeBeforeInjection = getParam<double>("TimeLoop.TimeBeforeInjection");
    const auto dtFactor = getParam<double>("TimeLoop.DtFactor", 1.0); // a factor for time convergence tests
    const auto initDtInjection = getParam<double>("TimeLoop.InitDtInjection")*dtFactor;

    //! instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<double>>(0.0, timeBeforeInjection, tEnd);

    // the assembler with time loop for instationary problem
    using Assembler = MultiDomainFVAssembler<TransportMDTraits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(bulkTransportProblem, lowDimTransportProblem),
                                                 std::make_tuple(bulkGridGeometry, lowDimGridGeometry),
                                                 std::make_tuple(bulkGridVariables, lowDimGridVariables),
                                                 couplingManager,
                                                 timeLoop, oldSol);

    // the linear solver
    using LinearSolver = BlockDiagILU0BiCGSTABSolver;
    auto linearSolver = std::make_shared<LinearSolver>();

    const bool computeLeakedContrastAgent = getParam<bool>("Problem.ComputeLeakedContrastAgent", false);
    double contrastAgentMassInjected = 0.0;

    ////////////////////////////////////////////////////////////
    // solve for the instationary contrast agent field
    ////////////////////////////////////////////////////////////

    //! start the time loop
    timeLoop->start(); do
    {
        Dune::Timer assembleTimer(false), solveTimer(false), updateTimer(false);
        std::cout << "\nAssembling and solving linear system ("
                  << linearSolver->name() << ") ... " << std::flush;

        // assemble stiffness matrix
        assembleTimer.start();
        lowDimTransportProblem->setTime(timeLoop->time() + timeLoop->timeStepSize());
        couplingManager->updateSolution(sol);
        assembler->assembleJacobianAndResidual(sol);
        assembleTimer.stop();

        // solve linear system
        solveTimer.start();
        auto deltaSol = sol;
        const bool converged = linearSolver->solve(assembler->jacobian(), deltaSol, assembler->residual());
        if (!converged) DUNE_THROW(Dune::MathError, "Linear solver did not converge!");
        solveTimer.stop();

        // update variables
        updateTimer.start();
        sol -= deltaSol;
        couplingManager->updateSolution(sol);
        bulkGridVariables->update(sol[bulkIdx]);
        lowDimGridVariables->update(sol[lowDimIdx]);
        updateTimer.stop();

        std::cout << "done.\n";
        const auto elapsedTot = assembleTimer.elapsed() + solveTimer.elapsed() + updateTimer.elapsed();
        std::cout << "Assemble/solve/update time: "
                  <<  assembleTimer.elapsed() << "(" << 100*assembleTimer.elapsed()/elapsedTot << "%)/"
                  <<  solveTimer.elapsed() << "(" << 100*solveTimer.elapsed()/elapsedTot << "%)/"
                  <<  updateTimer.elapsed() << "(" << 100*updateTimer.elapsed()/elapsedTot << "%)"
                  << "\n";

        // make the new solution the old solution
        oldSol = sol;
        bulkGridVariables->advanceTimeStep();
        lowDimGridVariables->advanceTimeStep();

        // output the source terms
        // bulkTransportProblem->computeSourceIntegral(sol[bulkIdx], *bulkGridVariables);
        // lowDimTransportProblem->computeSourceIntegral(sol[lowDimIdx], *lowDimGridVariables);

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // write vtk output
        if (enableVtkOutput)
        {
            bulkVtkWriter.write(timeLoop->time());
            lowDimVtkWriter.write(timeLoop->time());
        }

        // generate the MR signal
        mrSignalProcessor.generateSignal(timeLoop->time(), sol, *bulkGridVariables, *lowDimGridVariables, *couplingManager);

        // maybe compute the injection contrast agent mass in this time step
        if (computeLeakedContrastAgent)
        {
            contrastAgentMassInjected +=
                lowDimTransportProblem->computeContrastAgentMassInjected(sol[lowDimIdx], *lowDimGridVariables, timeLoop->timeStepSize());
        }

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // the first time step is just the time before injection where nothing happens
        // then we start injection
        const auto t = timeLoop->time() - timeBeforeInjection;
        timeLoop->setTimeStepSize( std::log(t + 1.0 + initDtInjection) * dtFactor );

    } while (!timeLoop->finished());

    ////////////////////////////////////////////////////////////
    // finalize, time to say goodbye
    ////////////////////////////////////////////////////////////

    const bool gnuplotPlotSignal = getParam<bool>("Problem.GnuplotPlotSignal", false);
    if (gnuplotPlotSignal)
        mrSignalProcessor.plotSignal();

    timeLoop->finalize(bulkGridView.comm());

    std::cout << "Transport computation took " << timer.elapsed() - flowTime - gridSetupTime << " seconds." << std::endl;
    std::cout << "Simulation took " << timer.elapsed() << " seconds." << std::endl;

    // optionally output the amount of constrast agent in the extra-vascular space
    if (computeLeakedContrastAgent)
    {
        const auto mLeaked = bulkTransportProblem->computeContrastAgentMass(sol[bulkIdx], *bulkGridVariables);
        std::cout << "Total leaked mass:   " << mLeaked << " kg." << std::endl;
        std::cout << "Total injected mass: " << contrastAgentMassInjected << " kg." << std::endl;
        std::cout << "Leaked ratio: " << mLeaked/contrastAgentMassInjected*100 << " %." << std::endl;
    }

    if (!verbose)
    {
        // reset output buffer
        std::cout.rdbuf(coutBuffer);
        std::cerr.rdbuf(cerrBuffer);
    }

    // compute the signal and return
    std::vector<double> t(80, 1.4); t[0] = 0.0;
    std::partial_sum(t.begin(), t.end(), t.begin());

    return mrSignalProcessor.getSignal(t);
}

} // end namespace Dumux

#endif
