// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_TISSUE_PROBLEM_HH
#define DUMUX_TISSUE_PROBLEM_HH

#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

template <class TypeTag>
class TissueProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using ResidualVector = Dumux::NumEqVector<PrimaryVariables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<PrimaryVariables::size()>;
    using PointSource = GetPropType<TypeTag, Properties::PointSource>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimworld = GridView::dimensionworld;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    TissueProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                  std::shared_ptr<CouplingManager> couplingManager,
                  const std::string& paramGroup = "")
    : ParentType(gridGeometry, paramGroup)
    , couplingManager_(couplingManager)
    {
        //read parameters from input file
        name_ = getParam<std::string>("Problem.Name") + "_3d_flow";
        lp_ = getParam<Scalar>("VesselWall.FiltrationCoefficient");
        sigma_ = getParam<Scalar>("VesselWall.ReflectionCoefficient");
        lesionIntensity_ = getParam<Scalar>("Problem.LesionIntensity");
        robinBoundary_ = getParam<bool>("Problem.RobinBoundary");
    }

    const std::string& name() const
    { return name_; }

    Scalar temperature() const
    { return 273.15 + 37; } // in [K]

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        // values.setAllDirichlet();
        return values;
    }

    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    { return initialAtPos(globalPos); }

    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return PrimaryVariables({1.0e5 + 933}); }

    template<class ElementVolumeVariables, class ElementFluxVarsCache>
    ResidualVector neumann(const Element& element,
                           const FVElementGeometry& fvGeometry,
                           const ElementVolumeVariables& elemVolVars,
                           const ElementFluxVarsCache& fluxCache,
                           const SubControlVolumeFace& scvf) const
    {
        ResidualVector values(0.0);
        if (robinBoundary_)
        {
            const auto farFieldPressure = 1.0e5 + 933; // 7 mmHg
            const auto elemSol = elementSolution(element, elemVolVars, fvGeometry);
            const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
            values[Indices::conti0EqIdx] = - (farFieldPressure - elemVolVars[0].pressure())/1e-4
                                             *elemVolVars[0].density()/elemVolVars[0].viscosity()
                                             *this->spatialParams().permeability(element, insideScv, elemSol);
        }
        return values;
    }

    void addPointSources(std::vector<PointSource>& pointSources) const
    { pointSources = this->couplingManager().bulkPointSources(); }

    template<class ElementVolumeVariables>
    void pointSource(PointSource& source,
                     const Element &element,
                     const FVElementGeometry& fvGeometry,
                     const ElementVolumeVariables& elemVolVars,
                     const SubControlVolume &scv) const
    {
        // compute source at every integration point
        const Scalar pressure3D = this->couplingManager().bulkPriVars(source.id())[Indices::pressureIdx];
        const Scalar pressure1D = this->couplingManager().lowDimPriVars(source.id())[Indices::pressureIdx];

        // correct the pressures by a constant oncotic / osmotic pressure
        static constexpr Scalar pOsmotic3D = 666.0; // -5mmHg
        static constexpr Scalar pOsmotic1D = 3300.0; // -25mmHg

        // get the segment radius
        const Scalar radius = this->couplingManager().radius(source.id());

        // calculate the source
        const Scalar meanRho = 1000;
        const Scalar liquidSource = -2*M_PI*radius*lp_*(pressure3D - pOsmotic3D - pressure1D + pOsmotic1D) * meanRho;
        source = liquidSource*source.quadratureWeight()*source.integrationElement();
    }

    //! Called after every time step
    //! Output the total global exchange term
    void computeSourceIntegral(const SolutionVector& sol, const GridVariables& gridVars)
    {
        PrimaryVariables source(0.0);
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVars.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, sol);

            for (auto&& scv : scvs(fvGeometry))
            {
                auto pointSources = this->scvPointSources(element, fvGeometry, elemVolVars, scv);
                pointSources *= scv.volume()*elemVolVars[scv].extrusionFactor();
                source += pointSources;
            }
        }

        std::cout << "Global integrated source (3D): " << source << '\n';
    }

    // compute volume sources as input for the transport model
    std::vector<Scalar> computeVolumeSources(const SolutionVector& sol, const GridVariables& gridVars)
    {
        const auto& gg = this->gridGeometry();
        std::vector<Scalar> volumeSource(this->couplingManager().pointSourceData().size(), 0.0);

        for (const auto& element : elements(gg.gridView()))
        {
            auto fvGeometry = localView(gg);
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVars.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, sol);

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto key = std::make_pair(gg.elementMapper().index(element), scv.indexInElement());
                if (this->pointSourceMap().count(key))
                {
                    auto pointSources = this->pointSourceMap().at(key);
                    for (auto&& pointSource : pointSources)
                    {
                        this->pointSource(pointSource, element, fvGeometry, elemVolVars, scv);
                        volumeSource[pointSource.id()] = pointSource.values();
                    }
                }
            }
        }

        return volumeSource;
    }

    // compute volume fluxes as input for the transport model
    std::vector<Scalar> computeVolumeFluxes(const SolutionVector& sol, const GridVariables& gridVars)
    {
        const auto& gg = this->gridGeometry();
        std::vector<Scalar> volumeFlux(gg.numScvf(), 0.0);

        using FluxVariables =  GetPropType<TypeTag, Properties::FluxVariables>;
        auto upwindTerm = [](const auto& volVars) { return volVars.mobility(Indices::conti0EqIdx); };

        for (const auto& element : elements(gg.gridView()))
        {
            auto fvGeometry = localView(gg);
            fvGeometry.bind(element);

            auto elemVolVars = localView(gridVars.curGridVolVars());
            elemVolVars.bind(element, fvGeometry, sol);

            auto elemFluxVars = localView(gridVars.gridFluxVarsCache());
            elemFluxVars.bind(element, fvGeometry, elemVolVars);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                const auto idx = scvf.index();

                if (!scvf.boundary())
                {
                    FluxVariables fluxVars;
                    fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVars);
                    volumeFlux[idx] = fluxVars.advectiveFlux(Indices::conti0EqIdx, upwindTerm);
                }
                else
                {
                    const auto bcTypes = this->boundaryTypes(element, scvf);
                    if (bcTypes.hasOnlyDirichlet()) // Dirichlet
                    {
                        FluxVariables fluxVars;
                        fluxVars.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVars);
                        volumeFlux[idx] = fluxVars.advectiveFlux(Indices::conti0EqIdx, upwindTerm);
                    }

                    else // Neumann
                    {
                        volumeFlux[idx] = neumann(element, fvGeometry, elemVolVars, 0.0, scvf)[Indices::pressureIdx]
                                          / elemVolVars[0].density(); // volume flux from mass flux
                    }
                }
            }
        }

        return volumeFlux;
    }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    static constexpr Scalar eps_ = 1.5e-7;
    std::string name_;

    Scalar lp_, sigma_;
    Scalar lesionIntensity_;
    bool robinBoundary_;

    std::shared_ptr<CouplingManager> couplingManager_;
};

} //end namespace Dumux

#endif
