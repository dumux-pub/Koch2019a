## Setup using PEST steps:

* Create a tpl (template file) from input file
* Check tpl file with tempcheck *.tpl (creates parameter file *.pmt (not necessary))
* Create instruction file (*ins) on how to read model output
* Check ins file with inscheck *.ins (creates observation parameter file *.obf (not necessary))
* Check ins file with inscheck *.ins model.out (creates observation parameter file *.obf with read out data (not necessary))
* To use pestgen to create the pest control file we need
  - Observation file (*.obs)
  - Parameter file (*.par)
* Then create the sample pest control file with pestgen <name> parfile obsfile
* Mark parameters as fixed (second param) if they should stay constant
* 3rd/4th/5th entry are initial, min, and max value


## Using script setup_pest.py

* Copy executbale and input file in this folder (same basename)
* Create a file <basename>.obs_raw with the patient data (normalized to 1, 80 data points)
* Call ./setup_pest.py <basename>
   -  Creates the pest control file and all other needed files
* Run pest <basename>
* The best fit is in a file called <basename>.par
* To plot the best fit use: ./plot_result.py mrsignal.out <basename>.obs_raw
