#!/usr/bin/env python3

import glob
import sys
import numpy as np
from numpy import genfromtxt
import os
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
plt.style.use('ggplot')

# plot the param study data
fig, axes = plt.subplots(1, 1, dpi=300, figsize=(12, 8))

font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 26}

font2 = {'family' : 'normal',
         'weight' : 'normal',
         'size'   : 16}

plt.rc('font', **font2)

import subprocess

ds = np.logspace(-11, -5, 50)
leaked = []
leaked_ratio = []

# for d in ds:
#     result = subprocess.run(['./msbern', '-Problem.ComputeLeakedContrastAgent', 'true', '-VesselWall.DiffusionCoefficient', str(d)], stdout=subprocess.PIPE)
#     log = result.stdout.decode('utf-8')
#     for line in log.split('\n'):
#         if "Total leaked mass" in line:
#             leaked.append(float(line.split()[3]))
#             print(d, " -> ", leaked[-1])
#         if "Leaked ratio" in line:
#             leaked_ratio.append(float(line.split()[2]))
#
# with open('log_d_leaked.txt', 'w') as outfile:
#     for d, m, r in zip(ds, leaked, leaked_ratio):
#         outfile.write("{} {} {}\n".format(d, m, r))

data = np.genfromtxt("log_d_leaked.txt")
ds = data[:,0]
leaked = data[:,1]*1e12
leaked_ratio = data[:,2]

axes2 = axes.twinx()
axes.plot(ds, leaked, linewidth=5, color="k")
axes2.plot(ds, leaked_ratio, linewidth=5, color="k")

axes.set_xlabel("$D_\omega$ in m/s", fontsize=font['size'], fontweight=font['weight'])
axes.set_ylabel("$m^c_t$ in ng", fontsize=font['size'], fontweight=font['weight'])
axes2.set_ylabel("$m^c_t$/$m_{inj}$ in %", fontsize=font['size'], fontweight=font['weight'])
axes.tick_params(axis='both', which='major', labelsize=20)
axes.tick_params(axis='both', which='minor', labelsize=18)
axes.set_xscale("log")

axes2.tick_params(axis='both', which='major', labelsize=20)
axes2.tick_params(axis='both', which='minor', labelsize=18)
axes2.set_xscale("log")

axes2.set_yticks(np.linspace(0, axes2.get_ybound()[1]/axes.get_ybound()[1]*1.3, 8))
axes.set_yticks(np.linspace(0, 1.3, 8))
axes2.yaxis.set_major_formatter(plticker.FormatStrFormatter("%.2f"))
axes.yaxis.set_major_formatter(plticker.FormatStrFormatter("%.2f"))

fig.tight_layout(rect=[0, 0.03, 1, 0.95], pad=0.4, w_pad=0.5, h_pad=2.0)
fig.savefig("d_leaked.pdf")
plt.show()
