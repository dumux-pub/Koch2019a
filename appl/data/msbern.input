# PEST Template file for MS-Bern DuMux input file
# All replacement fields are marked with question marks

[MixedDimension]
NumCircleSegments = 20
IntegrationOrder = 2

[TimeLoop]
TEnd = 112 # [s]
MaxTimeStepSize = 10.0 # [s]
InitDtInjection = 0.05 # [s]
TimeBeforeInjection = 22.0 # [s]

[Tissue.Grid]
LowerLeft = 0 0 0
UpperRight = 150e-6 160e-6 140e-6
Cells = 20 20 20

[Vessel.Grid]
File = data/ratbrain.dgf
MaxElementLengthFactor = 1.0

[Problem]
EnableGravity = false
Name = ms
FlowFileName = flow
SolveFlow = true
RobinBoundary = false

# BBB lesion intensity [0,1]
LesionIntensity = 1.0

# Plot options
PlotInflowCurve = false
InflowPlotSamples = 400
InflowPlotMinMaxTime = 0 160
PlotSignal = false
GnuplotShow = false
GnuplotOutputDirectory = "."

[Vtk]
EnableOutput = false
AddVelocity = false

[SpatialParams]
PermeabilityTissue = 8.3e-18 # [m^2]
PorosityTissue = 0.3 # [-]
Tortuosity = 0.35 # [-] (Sykova 2008 Diffusion in Brain Extracellular Space)

[1.Component]
Name = Blood
MolarMass = 0.018
LiquidKinematicViscosity = 3.0e-6 # [Pa*s]
LiquidDensity = 1050

[2.Component]
Name = InterstitialFluid
MolarMass = 0.018
LiquidKinematicViscosity = 1.3e-6 # [Pa*s]
LiquidDensity = 1030

[ContrastAgent]
Name = Gadobutrol
MolarMass = 0.018 # set to MolarMass for tracer, else 0.60471
DiffusionCoefficient = 3.25e-10 # estimated with Stokes-Einstein-Radius of 7A°

[BoundaryConditions1D]
PressureInput = 103500
DeltaPressure = 1000

[LinearSolver]
#ResidualReduction = 1e-30
#PreconditionerIterations = 2
MaxIterations = 20000

########################################################################
# Epsilons for assembly

[TissueFlow.Assembly.NumericDifference]
PriVarMagnitude = 1e5
BaseEpsilon = 1e10

[TissueTransport.Assembly.NumericDifference]
PriVarMagnitude = 1e-3
BaseEpsilon = 1e-2

[BloodFlow.Assembly.NumericDifference]
PriVarMagnitude = 1e5
BaseEpsilon = 1e10

[BloodTransport.Assembly.NumericDifference]
PriVarMagnitude = 1e-3
BaseEpsilon = 1e-2

########################################################################

[VesselWall]
# Filtration coefficient Lp [m²] # 1e-10
FiltrationCoefficient =                  9.746174233413226E-011

# Diffusion coefficient D of wall # 1e-13
DiffusionCoefficient =                      4.754947854870299E-08

ReflectionCoefficient = 0 # 1 means everything reflected

########################################################################

[MRSignal]
S0 = 1.0           # base signal (proton density, MR machine properties)
r1 = 3.2           # molecular T1 relaxivity Gadovist in m³/(mol*s)
r2 = 3.9           # molecular T2 relaxivity Gadovist in m³/(mol*s)
TE = 0.029         # echo time in sec
TR = 1.400         # repetition time in sec

# weighting of local susceptibility difference
# effect of vascular structure and tissue # 20
KappaB =                      3.202480256948131E+001

# weighting of local susceptibility difference effect inside tissure structure
KappaT =                      9.846818303577909E-002

# T1 relaxation time constant tissue (without contrast agent) in sec (at 3 Tesla field strength) # 1.5
T1TissuePreContrast =                      1.570114234734838E+000

# T2* relaxation time constant tissue (without contrast agent) in sec (at 3 Tesla field strength) # 0.08
T2TissuePreContrast =                      8.000000000000000E-002

########################################################################

[InflowCurve]
# scaling parameter mol*s/m³ (amplitude of scaling curve) # 100
a =                      3.812619924254471E+001

# average concentration human 5l blood volume 10ml Gadovist 1 mol/l = 2 mol/m³
b =                      1.934938693976044E+000

# time to peak from bolus arrival in sec (width of concentration curve) # 6
tp =                      5.000000000000000E+000

########################################################################
