DuMuX-MRI
==========

A dune module with the software and data to reproduce the results of the following publication:

T. Koch, B. Flemisch, R. Helmig, R. Wiest, D. Obrist,
_A multi-scale sub-voxel perfusion model to estimate diffusive capillary wall conductivity in multiple sclerosis lesions from perfusion MRI data_,
https://doi.org/10.1002/cnm.3298.


License
=========
GNU GPL-3 or later


Dependencies
==============

This is needed for manual installation. Also consult the `Dockerfile` for how to install
all dependencies starting from a fresh OS.

The module depends on the following other dune modules and the specified versions

* dune-common               releases/2.8
* dune-geometry             releases/2.8
* dune-grid                 releases/2.8
* dune-localfunctions       releases/2.8
* dune-istl                 releases/2.8
* dune-foamgrid             releases/2.8
* dumux                     releases/3.5

You further need the following python packages

* python (tested with version 3.6.7)
* matplotlib (tested with version 3.0.2)
* emcee (tested with version 3.0rc1)
* corner (test with version 2.0.1)

For the parameter estimation, you need the parameter estimation toolbox

* PEST Version 13.6. Watermark Numerical Computing.

For basic support on Ubunutu (in particular you need UMFPack, e.g. via SuiteSparse)

* sudo apt install cmake pkg-config build-essential gfortran libsuitesparse-dev

Installation instructions
===========================

Clone dune module repositories (and this repository)
```
git clone -b releases/2.8 https://gitlab.dune-project.org/core/dune-common.git
git clone -b releases/2.8 https://gitlab.dune-project.org/core/dune-geometry.git
git clone -b releases/2.8 https://gitlab.dune-project.org/core/dune-grid.git
git clone -b releases/2.8 https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone -b releases/2.8 https://gitlab.dune-project.org/core/dune-istl.git
git clone -b releases/2.8 https://gitlab.dune-project.org/extensions/dune-foamgrid.git
git clone -b releases/3.5 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Koch2019a.git
```

Configure and build dependencies
```
./dune-common/bin/dunecontrol --opts=Koch2019a/cmake.opts configure
./dune-common/bin/dunecontrol --opts=Koch2019a/cmake.opts make -j
```

Build and run application
```
cd dumux-mri/build-cmake/appl
make pymsbern
./run_and_plot.py
```

The script `run_and_plot.py` also allows to modify the basic model parameters
to simulate different MRI signal responses. All runtime parameters can be modified in `data/msbern.input`.
You can copy this file and use your version in the Python script. (See also `appl/src/msbern_python.cc` for some instructions on how to change parameters from Python.)
