// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_TISSUE_TRANSPORT_PROBLEM_HH
#define DUMUX_TISSUE_TRANSPORT_PROBLEM_HH

#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

template <class TypeTag>
class TissueTransportProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using ResidualVector = Dumux::NumEqVector<PrimaryVariables>;
    using EffectiveDiffusivityModel = GetPropType<TypeTag, Properties::EffectiveDiffusivityModel>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<PrimaryVariables::size()>;
    using PointSource = GetPropType<TypeTag, Properties::PointSource>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimworld = GridView::dimensionworld;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    static constexpr int phaseIdx = 0;
    static constexpr int compIdx = 0;

public:
    TissueTransportProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                           std::shared_ptr<CouplingManager> couplingManager,
                           const std::string& paramGroup = "")
    : ParentType(gridGeometry, paramGroup)
    , couplingManager_(couplingManager)
    {
        //read parameters from input file
        name_ = getParam<std::string>("Problem.Name") + "_3d_transport";
        lp_ = getParam<Scalar>("VesselWall.FiltrationCoefficient");
        lc_ = getParam<Scalar>("VesselWall.DiffusionCoefficient"); // m/s
        diffCoeff_ = 1.5e-11; // m^2/s
        sigma_ = getParam<Scalar>("VesselWall.ReflectionCoefficient");
        lesionIntensity_ = getParam<Scalar>("Problem.LesionIntensity");
        robinBoundary_ = getParam<bool>("Problem.RobinBoundary");
    }

    const std::string& name() const
    { return name_; }

    Scalar temperature() const
    { return 273.15 + 37; } // in [K]

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        // values.setAllDirichlet();
        return values;
    }

    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    { return initialAtPos(globalPos); }

    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return PrimaryVariables({0.0}); }

    template<class ElementVolumeVariables, class ElementFluxVarsCache>
    ResidualVector neumann(const Element& element,
                           const FVElementGeometry& fvGeometry,
                           const ElementVolumeVariables& elemVolVars,
                           const ElementFluxVarsCache& fluxCache,
                           const SubControlVolumeFace& scvf) const
    {
        ResidualVector values(0.0);
        if (robinBoundary_)
        {
            // const auto farFieldPressure = 1.0e5 + 933; // 7 mmHg
            const auto& volVars = elemVolVars[scvf.insideScvIdx()];
            values[0] = - (0.0 - volVars.moleFraction(0, compIdx))/1e-4
                          *volVars.molarDensity()
                          *volVars.effectiveDiffusionCoefficient(phaseIdx, phaseIdx, compIdx);
        }
        return values;
    }

    void addPointSources(std::vector<PointSource>& pointSources) const
    { pointSources = this->couplingManager().bulkPointSources(); }

    template<class ElementVolumeVariables>
    void pointSource(PointSource& source,
                     const Element &element,
                     const FVElementGeometry& fvGeometry,
                     const ElementVolumeVariables& elemVolVars,
                     const SubControlVolume &scv) const
    {
        // compute source at every integration point
        const Scalar x1D = this->couplingManager().lowDimPriVars(source.id())[compIdx];
        const Scalar x3D = this->couplingManager().bulkPriVars(source.id())[compIdx];

        // get the segment radius
        const Scalar radius = this->couplingManager().radius(source.id());

        const Scalar meanRhoMolar = 1000/0.018;
        auto transmuralFlux = -2*M_PI*radius*lc_*meanRhoMolar*(x3D - x1D)*source.quadratureWeight()*source.integrationElement();

        // advective transmural transport
        const auto liquidFlux = this->spatialParams().volumeSource(source.id());
        if (liquidFlux > 0)
            transmuralFlux += (1.0-sigma_)*liquidFlux/0.018*x1D;
        else
            transmuralFlux += (1.0-sigma_)*liquidFlux/0.018*x3D;

        transmuralFlux *= lesionIntensity_;
        source = transmuralFlux;
    }

    //! Called after every time step
    //! Output the total global exchange term
    void computeSourceIntegral(const SolutionVector& sol, const GridVariables& gridVars)
    {
        PrimaryVariables source(0.0);
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVars.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, sol);

            for (auto&& scv : scvs(fvGeometry))
            {
                auto pointSources = this->scvPointSources(element, fvGeometry, elemVolVars, scv);
                pointSources *= scv.volume()*elemVolVars[scv].extrusionFactor();
                source += pointSources;
            }
        }

        std::cout << "Global integrated source (3D): " << source << '\n';
    }

    //! Compute the current contrast agent mass in the whole domain
    Scalar computeContrastAgentMass(const SolutionVector& sol, const GridVariables& gridVars)
    {
        Scalar mass(0.0);
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVars.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, sol);

            for (auto&& scv : scvs(fvGeometry))
            {
                const auto& volVars = elemVolVars[scv];
                mass += 0.604715*volVars.porosity()*volVars.moleFraction(0, compIdx)*volVars.molarDensity(0)*scv.volume()*volVars.extrusionFactor();
            }
        }

        return mass;
    }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    static constexpr Scalar eps_ = 1.5e-7;
    std::string name_;

    Scalar lp_, lc_, sigma_, diffCoeff_;
    Scalar lesionIntensity_;
    bool robinBoundary_;

    std::shared_ptr<CouplingManager> couplingManager_;
};

} //end namespace Dumux

#endif
