#!/usr/bin/env python3
# parametrized network model

import matplotlib.pyplot as plt
from numpy import genfromtxt
from math import *
import numpy as np
import subprocess
import sys
import os
import errno
import copy
import multiprocessing as mp
import glob
plt.style.use('ggplot')

def make_program_call(executable, args):
    call = ['./' + executable]
    for k, v in args.items():
        call += ['-' + k, v]
    return call

def maybe_create_dir(filename):
    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

# do the parameter study for one parameter
# name: name of the parameter as in input file
# values: list of values for param to run
# fileprefix: the folder where to put the results
def do_study(study):

    name = study.name
    values = study.values
    fileprefix = study.fileprefix
    default_args = study.default_args

    # make a deep copy
    args = copy.deepcopy(default_args)

    # output folder
    args["Problem.GnuplotOutputDirectory"] = fileprefix
    args["Problem.GnuplotPlotSignal"] = "true"

    # create dir if not existent
    maybe_create_dir(fileprefix + "/")

    # output the default args
    with open(fileprefix + "/params.input" , "w") as file:
        for key, value in args.items():
            file.write("{} {}\n".format(key, value))

    # output data
    data = {}

    if not study.only_plot:
        print("\033[1;32m {:.0f}% \033[m ({})".format(0, name))

        for iteration, value in enumerate(values):
            args[name] = str(value)
            call = make_program_call(executable, args)
            #print("Calling command ", " ".join(call))
            devnull = open(os.devnull, 'w')
            # subprocess.call(call, stdout=devnull, stderr=devnull)
            subprocess.call(call)
            # read the signal plot data
            date = genfromtxt(fileprefix + "/mrsignal.dat", delimiter=' ')
            data[value] = {'time':date[:,0], 'signal':date[:,1]}

            print("\033[1;32m {:.0f}% \033[m ({})".format(float(iteration+1)/float(len(values))*100, name))

        # write data to file
        for value, d in data.items():
            filename = fileprefix + "/" + str(value) + ".dat"
            with open(filename, "w") as file:
                for t, s in zip(d['time'], d['signal']):
                    file.write(str(t) + " " + str(s) + "\n")

    # we only want to plot the existing files
    else:
        for filename in glob.glob(os.path.join(fileprefix, '*.dat')):
            data_name = os.path.basename(filename).rstrip(".dat")
            if data_name == "mrsignal":
                continue
            date = genfromtxt(filename, delimiter=' ')
            data[data_name] = {'time':date[:,0], 'signal':date[:,1]}


    # plot the param study data
    fig = plt.figure(dpi=72, figsize=(12, 8))

    font = {'family' : 'normal',
            'weight' : 'bold',
            'size'   : 16}

    font2 = {'family' : 'normal',
             'weight' : 'normal',
             'size'   : 14}

    plt.rc('font', **font2)

    plt.title('simulated MR signal for different {}\n'.format(name), fontsize=font['size'], fontweight=font['weight'])
    plt.ylabel('MR signal (normalized to baseline)', fontsize=font['size'], fontweight=font['weight'])
    plt.xlabel('time in s', fontsize=font['size'], fontweight=font['weight'])

    for value, d in data.items():
        plt.plot(d['time'], d['signal'], linewidth=2.0, label=str(value))

    plt.xlim([0, 200])
    plt.legend(loc='upper right')
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    fig.savefig(fileprefix + "/signals.png")

    plt.show(block=False)


class study(object):
    def __init__(self, name, values, fileprefix, default_args):
        self.name = name
        self.values = values
        self.fileprefix = fileprefix
        self.default_args = default_args
        self.only_plot = False

# script
if __name__ == '__main__':

    executable = "msbern"
    pre = "healthy_"

    # the default arguments
    default_args = {}
    default_args["ParameterFile"] = "msbern.input"
    default_args["Problem.LesionIntensity"] = "1" # healthy: 0, sick: 1

    # sick best fit
    # default_args["MRSignal.T1TissuePreContrast"] = "1.761906100863405" # seconds
    # default_args["InflowCurve.a"] = "30.08259812406274" # scaling params mol*s/m3
    # default_args["InflowCurve.b"] = "1.199806902816779" # baseline mol/m3
    # default_args["InflowCurve.tp"] = "4.754267263602821" # time to peak in s
    # default_args["VesselWall.FiltrationCoefficient"] = "7.2025924803156602E-12" # Lp
    # default_args["VesselWall.DiffusionCoefficient"] = "8.2049043864237265E-08" # D wall
    # default_args["MRSignal.KappaB"] = "14.19066477157780"
    # default_args["MRSignal.KappaT"] = "0.7286103053233263"

    # healthy best fit
    default_args["MRSignal.T1TissuePreContrast"] = "2.000000000000000" # seconds
    default_args["InflowCurve.a"] = "30.02539295055407" # scaling params mol*s/m3
    default_args["InflowCurve.b"] = "0.6125272328632249" # baseline mol/m3
    default_args["InflowCurve.tp"] = "6.032013710765214" # time to peak in s
    default_args["VesselWall.FiltrationCoefficient"] = "9.9999999999999998E-13" # Lp
    default_args["VesselWall.DiffusionCoefficient"] = "1.0111045577437230E-10" # D wall
    default_args["MRSignal.KappaB"] = "35.58571516953696"
    default_args["MRSignal.KappaT"] = "0.9985821514368615"

    default_args["Tissue.Grid.Cells"] = "20 "*3 #"100 100 100"
    default_args["TimeLoop.DtFactor"] = "1.0" #"0.25"
    default_args["MixedDimension.NumCircleSegments"] = "128" #"1.0"

    ranges = {}
    ranges["MRSignal.KappaB"] = np.linspace(0, 100, 5).tolist()
    ranges["MRSignal.KappaT"] = np.linspace(0, 50, 5).tolist()
    ranges["VesselWall.DiffusionCoefficient"] = np.logspace(-10, -6, 5).tolist()
    ranges["VesselWall.FiltrationCoefficient"] = np.logspace(-12, -9, 4).tolist() # very little influence
    ranges["InflowCurve.tp"] = np.linspace(1, 8, 5).tolist()
    ranges["InflowCurve.b"] = np.linspace(0, 2, 5).tolist()
    ranges["InflowCurve.a"] = np.linspace(10, 100, 5).tolist()
    ranges["MRSignal.T1TissuePreContrast"] = np.linspace(0.8, 3.0, 5).tolist()

    # ranges["Tissue.Grid.Cells"] = ["5 "*3, "10 "*3, "20 "*3, "30 "*3, "40 "*3, "60 "*3, "80 "*3]
    # ranges["TimeLoop.DtFactor"] = ["10.0", "5.0", "0.5", "1.0"]
    # ranges["MixedDimension.NumCircleSegments"] = ["4", "16", "32", "64"]

    for name, values in ranges.items():
        ranges[name] = ranges[name] + [default_args[name]]

    # file prefixes for all parameters
    prefixes = {}
    prefixes["MRSignal.T1TissuePreContrast"] = pre + "param_t1pre"
    prefixes["MRSignal.T2TissuePreContrast"] = pre + "param_t2pre"
    prefixes["InflowCurve.a"] = pre + "param_a"
    prefixes["InflowCurve.b"] = pre + "param_b"
    prefixes["InflowCurve.tp"] = pre + "param_tp"
    prefixes["VesselWall.FiltrationCoefficient"] = pre + "param_lp"
    prefixes["VesselWall.DiffusionCoefficient"] = pre + "param_d"
    prefixes["MRSignal.KappaB"] = pre + "param_kappaB"
    prefixes["MRSignal.KappaT"] = pre + "param_kappaT"
    prefixes["Tissue.Grid.Cells"] = pre + "param_hmax"
    prefixes["TimeLoop.DtFactor"] = pre + "param_dtfactor"
    prefixes["MixedDimension.NumCircleSegments"] = pre + "param_circleips"

    # make a list for mp
    studies = [study(name, values, prefixes[name], default_args) for name, values in ranges.items()]

    print ("Computing parameter study on {} threads.".format(mp.cpu_count()))

    with mp.Pool() as pool:
        pool.map(do_study, studies)

    # create pdf
    devnull = open(os.devnull, 'w')
    subprocess.call(['convert', 'param_*/*png', 'study.pdf'], stdout=devnull, stderr=devnull)
