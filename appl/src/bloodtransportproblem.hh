// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase blood flow model:
 * Blood is flowing through a 1d network grid.
 */
#ifndef DUMUX_BLOOD_FLOW_TRANSPORT_PROBLEM_HH
#define DUMUX_BLOOD_FLOW_TRANSPORT_PROBLEM_HH

#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/io/gnuplotinterface.hh>

namespace Dumux {

/*!
 * \ingroup OnePTests
 * \brief Exact solution 1D-3D
 */
template <class TypeTag>
class BloodTransportProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using PointSource = GetPropType<TypeTag, Properties::PointSource>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using ResidualVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<PrimaryVariables::size()>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimworld = GridView::dimensionworld;
    static constexpr int compIdx = 0;

public:
    template<class GridData>
    BloodTransportProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                          std::shared_ptr<CouplingManager> couplingManager,
                          std::shared_ptr<GridData> gridData,
                          const std::string& paramGroup = "")
    : ParentType(gridGeometry, paramGroup)
    , couplingManager_(couplingManager)
    {
        //read parameters from input file
        name_ = getParam<std::string>("Problem.Name") + "_1d_transport";
        p_in_ = getParam<Scalar>("BoundaryConditions1D.PressureInput");
        delta_p_ = getParam<Scalar>("BoundaryConditions1D.DeltaPressure");
        lp_ = getParam<Scalar>("VesselWall.FiltrationCoefficient");
        lc_    = getParam<Scalar>("VesselWall.DiffusionCoefficient"); // m/s
        sigma_ = getParam<Scalar>("VesselWall.ReflectionCoefficient");
        lesionIntensity_ = getParam<Scalar>("Problem.LesionIntensity");

        inflowA_ = getParam<Scalar>("InflowCurve.a");
        inflowB_ = getParam<Scalar>("InflowCurve.b");
        inflowTp_ = getParam<Scalar>("InflowCurve.tp");
        tStart_ = getParam<Scalar>("TimeLoop.TimeBeforeInjection");

        this->spatialParams().readGridParams(*gridData);

        if (getParam<bool>("Problem.PlotInflowCurve", false))
        {
            GnuplotInterface<Scalar> gnuplot(true);
            gnuplot.resetPlot();
            gnuplot.setXlabel("time in s");
            gnuplot.setYlabel("gadolinium concentration in mmol/ml");

            // do plot x over mu for these temperatures
            const auto samples = getParam<int>("Problem.InflowPlotSamples");
            const auto tminmax = getParam<std::vector<Scalar>>("Problem.InflowPlotMinMaxTime");
            gnuplot.setXRange(tminmax[0], tminmax[1]);

            const auto dt = (tminmax[1]-tminmax[0])/(samples-1);
            std::vector<Scalar> t(samples, dt), x(samples, 0.0);
            std::partial_sum(t.begin(), t.end(), t.begin());
            int n = 0;
            std::generate(x.begin(), x.end(), [&n,&t,this]()
            { return inflowConcentration_(t[n++])*1e-3; }); // convert from mol/m³ to mmol/ml

            gnuplot.addDataSetToPlot(t, x, "x_{gadolinum}");
            // plot all data interactively
            gnuplot.plot("inflow curve");
        }

        // mapping from element index to boundaryFlag
        setBoundaryDomainMarker_(*gridData);
    }

    template<class ElementSolution>
    Scalar extrusionFactor(const Element &element,
                           const SubControlVolume &scv,
                           const ElementSolution& elemSol) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        const auto radius = this->spatialParams().radius(eIdx);
        return M_PI*radius*radius;
    }

    const std::string& name() const
    { return name_; }

    Scalar temperature() const
    { return 273.15 + 37.0; } // Body temperature

    BoundaryTypes boundaryTypes(const Element &element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes bcTypes;
        bcTypes.setAllDirichlet();

        switch (boundaryDomainMarker_[scvf.insideScvIdx()])
        {
            case 1: // inflow boundaries
                bcTypes.setNeumann(compIdx);
                // bcTypes.setDirichlet(compIdx);
                break;
            case 2: // outflow boundaries
                bcTypes.setDirichlet(compIdx);
                break;
            default:
                DUNE_THROW(Dune::InvalidStateException,
                             "Unknown boundary marker for element " << scvf.insideScvIdx());
        }

        return bcTypes;
    }

    PrimaryVariables dirichlet(const Element &element, const SubControlVolumeFace &scvf) const
    { return PrimaryVariables(0.0); }

    template<class ElementVolumeVariables, class ElementFluxVarsCache>
    ResidualVector neumann(const Element& element,
                           const FVElementGeometry& fvGeometry,
                           const ElementVolumeVariables& elemVolVars,
                           const ElementFluxVarsCache& fluxCache,
                           const SubControlVolumeFace& scvf) const
    {
        ResidualVector values(0.0);
        const auto vel = this->spatialParams().velocity_estimate(scvf.insideScvIdx());
        const auto c = inflowConcentration_(time_);
        values[Indices::transportEqIdx] = -vel*c;
        return values;
    }

    void addPointSources(std::vector<PointSource>& pointSources) const
    { pointSources = this->couplingManager().lowDimPointSources(); }

    template<class ElementVolumeVariables>
    void pointSource(PointSource& source,
                     const Element &element,
                     const FVElementGeometry& fvGeometry,
                     const ElementVolumeVariables& elemVolVars,
                     const SubControlVolume &scv) const
    {
        // compute source at every integration point
        const Scalar x1D = elemVolVars[scv].moleFraction(0, compIdx);
        const Scalar x3D = this->couplingManager().bulkPriVars(source.id())[compIdx];

        // get the segment radius
        const Scalar radius = this->couplingManager().radius(source.id());

        const Scalar meanRhoMolar = 1000/0.018;
        auto transmuralFlux = 2*M_PI*radius*lc_*meanRhoMolar*(x3D - x1D)*source.quadratureWeight()*source.integrationElement();

        // advective transmural transport
        const auto liquidFlux = this->spatialParams().volumeSource(source.id());
        if (liquidFlux > 0)
            transmuralFlux += (1.0-sigma_)*liquidFlux/0.018*x3D;
        else
            transmuralFlux += (1.0-sigma_)*liquidFlux/0.018*x1D;

        transmuralFlux *= lesionIntensity_;
        source = transmuralFlux;
    }

    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return PrimaryVariables({0.0}); }

    //! Called after every time step
    //! Output the total global exchange term
    void computeSourceIntegral(const SolutionVector& sol, const GridVariables& gridVars)
    {
        PrimaryVariables source(0.0);
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVars.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, sol);

            for (auto&& scv : scvs(fvGeometry))
            {
                auto pointSources = this->scvPointSources(element, fvGeometry, elemVolVars, scv);
                pointSources *= scv.volume()*elemVolVars[scv].extrusionFactor();
                source += pointSources;
            }
        }

        std::cout << "Global integrated source (1D): " << source << '\n';
    }

    //! Compute the current contrast agent mass in the whole domain
    Scalar computeContrastAgentMassInjected(const SolutionVector& sol, const GridVariables& gridVars, Scalar dt)
    {
        Scalar mass(0.0);
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVars.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, sol);

            for (auto&& scvf : scvfs(fvGeometry))
            {
                if (scvf.boundary())
                {
                    const auto bcTypes = boundaryTypes(element, scvf);
                    if (bcTypes.hasNeumann())
                    {
                        const auto& volVars = elemVolVars[scvf.insideScvIdx()];
                        mass -= neumann(element, fvGeometry, elemVolVars, 0.0, scvf)[0]*scvf.area()*volVars.extrusionFactor()*0.604715*dt;
                    }
                }
            }
        }

        return mass;
    }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

    //! Set the correct time to evaluate the boundary condition
    void setTime(Scalar t)
    { time_ = t; }

private:

    template<class GridData>
    void setBoundaryDomainMarker_(const GridData& gridData)
    {
        const auto& gg = this->gridGeometry();
        boundaryDomainMarker_.resize(gg.gridView().size(0));

        for (const auto& element : elements(gg.gridView()))
        {
            if (!element.hasBoundaryIntersections())
                continue;

            auto ancestor = element;
            while (ancestor.hasFather())
                ancestor = ancestor.father();

            for (const auto& intersection : intersections(gg.gridView().grid().levelGridView(0), ancestor))
            {
                if (intersection.boundary())
                {
                    const auto vertex = ancestor.template subEntity<1>(intersection.indexInInside());
                    boundaryDomainMarker_[gg.elementMapper().index(element)] = gridData.parameters(vertex)[0];
                }
            }
        }
    }

    // time dependent inflow function
    Scalar inflowConcentration_(Scalar t) const
    {
        using std::max; using std::exp;
        if (t >= tStart_)
        {
            t = max(0.0, t - tStart_);
            return inflowA_*t/(inflowTp_*inflowTp_)*exp(-t/inflowTp_) + inflowB_*(1.0 - exp(-t/inflowTp_));
        }
        else
            return 0.0;
    }

    static constexpr Scalar eps_ = 1.5e-7;
    std::string name_;

    // boundary conditions
    Scalar p_in_;
    Scalar delta_p_;

    // parameters
    Scalar lesionIntensity_, lp_, lc_, sigma_;
    Scalar inflowA_, inflowB_, inflowTp_, tStart_;

    std::vector<int> boundaryDomainMarker_;

    std::shared_ptr<CouplingManager> couplingManager_;

    Scalar time_ = 0.0;
};

} //end namespace Dumux

#endif
