#!/usr/bin/env python3

import glob
import sys
import numpy as np
from numpy import genfromtxt
import os
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
plt.style.use('ggplot')

# plot the param study data
fig, axes = plt.subplots(2, 2, dpi=72, figsize=(20, 12))

font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 20}

font2 = {'family' : 'normal',
         'weight' : 'normal',
         'size'   : 20}

plt.rc('font', **font2)

folders = ['param_hmax', 'param_dtfactor']

titles = ['grid refinement', 'time step refinement']

idx = [0, 1]

# read data in dictionary
for index, (folder, title) in enumerate(zip(folders, titles)):
    data = {}
    for filename in glob.glob(os.path.join("./" + folder, '*.dat')):
        data_name = os.path.basename(filename).rstrip(".dat")
        if data_name == "mrsignal":
            continue
        date = genfromtxt(filename, delimiter=' ')
        value = str(data_name)
        data[value] = {'time':date[:,0], 'signal':date[:,1]}

    i = (0, idx[index])
    j = (1, idx[index])
    axes[i].set_title(title, fontsize=font['size'], fontweight=font['weight'])
    axes[i].set_xlabel('time in s', fontsize=font['size'], fontweight=font['weight'])
    axes[i].set_ylabel('NMR signal (normalized)', fontsize=font['size'], fontweight=font['weight'])

    data = sorted(data.items(), key=lambda d: float(d[0].split()[0]))
    for value, d in data:
        if i[1] == 0: # grid cells
            value = 150e-6 / float(value.split()[0])
        axes[i].plot(d['time'], d['signal'], '-', linewidth=3.0, label="{:.3e}".format(float(value)))

    error_space = []
    hmax = []
    error_time = []
    tfactor = []
    for value, d in data:
        ord = np.inf # l_inf norm over time
        if i[1] == 0: # grid cells
            if value == data[-1][0]:
                continue
            hmax.append(150e-6 / float(value.split()[0]))
            error_space.append(np.linalg.norm((d['signal']-data[-1][1]['signal']), ord=ord) / np.linalg.norm(data[-1][1]['signal'], ord=ord))
        else:
            if value == data[0][0]:
                continue
            tfactor.append(float(value))
            error_time.append(np.linalg.norm((d['signal']-data[0][1]['signal']), ord=ord) / np.linalg.norm(data[0][1]['signal'], ord=ord))

    # compute the average rate
    if i[1] == 0: # grid cells
        rates_space = []
        for n in range(0, len(error_space)-1):
            rates_space.append((np.log(error_space[n]) - np.log(error_space[n+1])) / (np.log(hmax[n]) - np.log(hmax[n+1])))
        print(rates_space)
        print(np.mean(rates_space))
        avg_rate_space = np.mean(rates_space)
    else:
        rates_time =  []
        for n in range(0, len(error_time)-1):
            rates_time.append((np.log(error_time[n]) - np.log(error_time[n+1])) / (np.log(tfactor[n]) - np.log(tfactor[n+1])))
        print(rates_time)
        print(np.mean(rates_time))
        avg_rate_time = np.mean(rates_time)

    if i[1] == 0: # grid cells
        axes[j].plot(hmax, error_space, '-', linewidth=5.0, marker='o', markersize=10, markeredgewidth=3, label=r"$L_{\infty}$ error")
        axes[j].set_xlim([1e-6, 5e-5])
        axes[j].plot([2.5e-6, 3e-5], [6e-3*2.5e-6/3e-5, 6e-3], '--', linewidth=5.0, label="1st order")
        axes[j].plot([2.5e-6, 3e-5], [3e-3*2.5e-6/3e-5*2.5e-6/3e-5, 3e-3], '--', linewidth=5.0, label="2nd order")
        axes[j].set_xlabel(r'$h_{max}$', fontsize=font['size'], fontweight=font['weight'])
        axes[j].set_ylabel(r'relative $L_{\infty}$ error', fontsize=font['size'], fontweight=font['weight'])
        bbox_props = dict(boxstyle="square,pad=0.3", fc="white", ec="white", lw=2)
        axes[j].text(2e-5, 3e-5, "mean rate = {:.4f}".format(avg_rate_space), ha="center", va="center", rotation=0, size=20, bbox=bbox_props)
    else:
        axes[j].plot(tfactor, error_time, '-', linewidth=5.0, marker='o', markersize=10, markeredgewidth=3, label=r"$L_{\infty}$ error")
        axes[j].set_xlim([0.4, 12.0])
        axes[j].plot([1.0, 10.0], [6e-3/10, 6e-3], '--', linewidth=5.0, label="1st order")
        axes[j].plot([1.0, 10.0], [3e-3/100, 3e-3], '--', linewidth=5.0, label="2nd order")
        axes[j].set_xlabel(r'time step factor $\theta$', fontsize=font['size'], fontweight=font['weight'])
        axes[j].set_ylabel(r'relative $L_{\infty}$ error', fontsize=font['size'], fontweight=font['weight'])
        bbox_props = dict(boxstyle="square,pad=0.3", fc="white", ec="white", lw=2)
        axes[j].text(5, 3e-5, "mean rate = {:.4f}".format(avg_rate_time), ha="center", va="center", rotation=0, size=20, bbox=bbox_props)

    axes[j].invert_xaxis()
    axes[j].set_xscale("log")
    axes[j].set_yscale("log")
    axes[j].set_ylim([1e-5, 1e-1])
    axes[j].legend(loc='upper right')
    axes[j].tick_params(labelsize=font["size"])

    axes[i].set_xlim([0, 160])
    axes[i].legend(loc='upper right')
    axes[i].tick_params(labelsize=font["size"])

fig.tight_layout(rect=[0, 0.03, 1, 0.95], pad=0.4, w_pad=0.5, h_pad=2.0)
fig.savefig("convergence.pdf")
plt.show()
