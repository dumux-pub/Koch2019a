#!/usr/bin/env python3
import sys
import os
import glob
import matplotlib.pyplot as plt
from numpy import genfromtxt
from numpy import mean
import numpy as np

if len(sys.argv) == 2:
    resname = str(sys.argv[1])
    obs, sim = genfromtxt(resname, skip_header=1, usecols=(2,3), unpack=True)
elif len(sys.argv) == 3:
    simname = str(sys.argv[1])
    obsname = str(sys.argv[2])
    sim = genfromtxt(simname)
    obs = genfromtxt(obsname)
else:
    print("Supply file to plot (<model>.res or <model>.out and <model>.obs_raw)")
    sys.exit(1)

t = [i*1.4 for i in range(0,80)]

# plot the param study data
fig = plt.figure(dpi=72, figsize=(8, 5))

font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 20}

font2 = {'family' : 'normal',
         'weight' : 'normal',
         'size'   : 20}

plt.rc('font', **font2)

plt.ylabel('NMR signal \n (normalized to baseline)', fontsize=font['size'], fontweight=font['weight'])
plt.xlabel('time in $s$', fontsize=font['size'], fontweight=font['weight'])

plt.plot(t, obs, "r--", linewidth=5.0, label="lesion")
plt.plot(t, sim, "b-", linewidth=5.0, label="NAWM")
ax = plt.gca()
ax.tick_params(axis='both', which='major', labelsize=18)
ax.tick_params(axis='both', which='minor', labelsize=18)

plt.xlim([0, 120])
plt.legend(loc='upper left')
plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0, rect=[0.05, 0, 1, 1])
plt.show()
