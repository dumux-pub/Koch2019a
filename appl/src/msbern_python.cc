// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief run the PDE forward model through Python
 *
 * You can use this from Python as follows
 *
 * import numpy as np
 * from pymsbern import run_forward_model
 *
 * params = {} // params is a dictionary of strings overwriting stuff in the input file
 * params["MRSignal.T1TissuePreContrast"] = "1.3"
 * params["InflowCurve.a"] = "38.0"
 * params["InflowCurve.b"] = "2.0"
 * params["InflowCurve.tp"] = "5.0"
 * params["VesselWall.DiffusionCoefficient"] = "4.7e-8"
 * params["MRSignal.KappaB"] = "32"
 * params["MRSignal.KappaT"] = "0.05"
 * input_file = "path_to_msbern.input"
 *
 * result_curve = np.array(run_forward_model(params=params, input=input_file, verbose=True))
 * print(result_curve) // should show 80 values of signal intensity over time
 */
#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/python/pybind11/pybind11.h>
#include <dune/python/pybind11/stl.h>

#include "msbern.hh"

// define a python interface for the model function so it can be run by python
PYBIND11_MODULE(pymsbern, m)
{
    namespace py = pybind11;

    m.doc() = "The PDE forward model"; // optional module docstring
    m.def("run_forward_model", &Dumux::runForwardModel, "Run one simulation of the PDE forward model",
          py::arg("params"), py::arg("input"), py::arg("verbose") = false);

    int argc = 0;
    char **argv = NULL;
    Dune::MPIHelper::instance(argc, argv);
}
