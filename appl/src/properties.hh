// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_MRI_PROPERTIES_HH
#define DUMUX_MRI_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>
#include <dune/foamgrid/foamgrid.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/reorderingdofmapper.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/porousmediumflow/1p/incompressiblelocalresidual.hh>
#include <dumux/porousmediumflow/tracer/model.hh>
#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidmatrixinteractions/diffusivityconstanttortuosity.hh>

#include <dumux/multidomain/embedded/couplingmanager1d3d_average.hh>

#include "bloodflowproblem.hh"
#include "bloodflowspatialparams.hh"
#include "bloodtransportproblem.hh"
#include "tissueproblem.hh"
#include "tissuetransportproblem.hh"
#include "tissuespatialparams.hh"

#include "tracerfluidsystem.hh"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/// BloodFlow model
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
namespace Dumux::Properties {

namespace TTag { struct BloodFlow { using InheritsFrom = std::tuple<OneP, CCTpfaModel>; }; }

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::BloodFlow>
{ using type = Dune::FoamGrid<1, 3>; };

// caching options
template<class TypeTag> struct EnableGridGeometryCache<TypeTag, TTag::BloodFlow> { static constexpr bool value = true; };
template<class TypeTag> struct EnableGridVolumeVariablesCache<TypeTag, TTag::BloodFlow> { static constexpr bool value = true; };
template<class TypeTag> struct EnableGridFluxVariablesCache<TypeTag, TTag::BloodFlow> { static constexpr bool value = true; };
template<class TypeTag> struct SolutionDependentAdvection<TypeTag, TTag::BloodFlow> { static constexpr bool value = false; };
template<class TypeTag> struct SolutionDependentMolecularDiffusion<TypeTag, TTag::BloodFlow> { static constexpr bool value = false; };
template<class TypeTag> struct SolutionDependentHeatConduction<TypeTag, TTag::BloodFlow> { static constexpr bool value = false; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::BloodFlow>
{ using type = BloodFlowProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::BloodFlow>
{ using type = BloodFlowSpatialParams<GetPropType<TypeTag, Properties::GridGeometry>, double>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::BloodFlow>
{ using type = FluidSystems::OnePLiquid<double, Components::Constant<2, double> >; };

// Set the local residual
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::BloodFlow>
{ using type = OnePIncompressibleLocalResidual<TypeTag>; };

// if we have pt scotch use the reordering dof mapper to optimally sort the dofs (cc)
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::BloodFlow>
{
private:
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;
    using ElementMapper = ReorderingDofMapper<GridView>;
    using VertexMapper = Dune::MultipleCodimMultipleGeomTypeMapper<GridView>;
    using MapperTraits = DefaultMapperTraits<GridView, ElementMapper, VertexMapper>;
public:
    using type = CCTpfaFVGridGeometry<GridView, enableCache, CCTpfaDefaultGridGeometryTraits<GridView, MapperTraits>>;
};

} // end namespace Dumux::Properties


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/// BloodTransport model
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
namespace Dumux::Properties {

namespace TTag { struct BloodTransport { using InheritsFrom = std::tuple<Tracer,CCTpfaModel>; }; }

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::BloodTransport>
{ using type = Dune::FoamGrid<1, 3>; };

template<class TypeTag> struct EnableGridGeometryCache<TypeTag, TTag::BloodTransport> { static constexpr bool value = true; };
template<class TypeTag> struct EnableGridVolumeVariablesCache<TypeTag, TTag::BloodTransport> { static constexpr bool value = true; };
template<class TypeTag> struct EnableGridFluxVariablesCache<TypeTag, TTag::BloodTransport> { static constexpr bool value = true; };
template<class TypeTag> struct SolutionDependentAdvection<TypeTag, TTag::BloodTransport> { static constexpr bool value = false; };
template<class TypeTag> struct SolutionDependentMolecularDiffusion<TypeTag, TTag::BloodTransport> { static constexpr bool value = false; };
template<class TypeTag> struct SolutionDependentHeatConduction<TypeTag, TTag::BloodTransport> { static constexpr bool value = false; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::BloodTransport>
{ using type = BloodTransportProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::BloodTransport>
{ using type = BloodFlowSpatialParams<GetPropType<TypeTag, Properties::GridGeometry>, double>; };

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::BloodTransport>
{ using type = TracerFluidSystem<double>; };

// Use molar balances and a mole fraction as primary variable
template<class TypeTag>
struct UseMoles<TypeTag, TTag::BloodTransport>
{ static constexpr bool value = true; };

// if we have pt scotch use the reordering dof mapper to optimally sort the dofs (cc)
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::BloodTransport>
{
private:
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = typename GetPropType<TypeTag, Properties::Grid>::LeafGridView;
    using ElementMapper = ReorderingDofMapper<GridView>;
    using VertexMapper = Dune::MultipleCodimMultipleGeomTypeMapper<GridView>;
    using MapperTraits = DefaultMapperTraits<GridView, ElementMapper, VertexMapper>;
public:
    using type = CCTpfaFVGridGeometry<GridView, enableCache, CCTpfaDefaultGridGeometryTraits<GridView, MapperTraits>>;
};

} // end namespace Dumux::Properties


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/// Tissue flow model
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
namespace Dumux::Properties {

namespace TTag { struct Tissue { using InheritsFrom = std::tuple<CCTpfaModel, OneP>; }; }

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::Tissue>
{ using type = Dune::YaspGrid<3, Dune::EquidistantOffsetCoordinates<double, 3>>; };

// caching options
template<class TypeTag> struct EnableGridGeometryCache<TypeTag, TTag::Tissue> { static constexpr bool value = true; };
template<class TypeTag> struct EnableGridVolumeVariablesCache<TypeTag, TTag::Tissue> { static constexpr bool value = true; };
template<class TypeTag> struct EnableGridFluxVariablesCache<TypeTag, TTag::Tissue> { static constexpr bool value = true; };
template<class TypeTag> struct SolutionDependentAdvection<TypeTag, TTag::Tissue> { static constexpr bool value = false; };
template<class TypeTag> struct SolutionDependentMolecularDiffusion<TypeTag, TTag::Tissue> { static constexpr bool value = false; };
template<class TypeTag> struct SolutionDependentHeatConduction<TypeTag, TTag::Tissue> { static constexpr bool value = false; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::Tissue>
{ using type = TissueProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::Tissue>
{ using type = TissueSpatialParams<GetPropType<TypeTag, Properties::GridGeometry>, double>; };

// Set the local residual
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::Tissue>
{ using type = OnePIncompressibleLocalResidual<TypeTag>; };

// the fluid system
template<class TypeTag> struct FluidSystem<TypeTag, TTag::Tissue>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};

} // end namespace Dumux::Properties


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/// Tissue transport model
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
namespace Dumux::Properties {

namespace TTag { struct TissueTransport { using InheritsFrom = std::tuple<Tracer, CCTpfaModel>; }; }

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TissueTransport>
{ using type = Dune::YaspGrid<3, Dune::EquidistantOffsetCoordinates<double, 3>>; };

// caching options
template<class TypeTag> struct EnableGridGeometryCache<TypeTag, TTag::TissueTransport> { static constexpr bool value = true; };
template<class TypeTag> struct EnableGridVolumeVariablesCache<TypeTag, TTag::TissueTransport> { static constexpr bool value = true; };
template<class TypeTag> struct EnableGridFluxVariablesCache<TypeTag, TTag::TissueTransport> { static constexpr bool value = true; };
template<class TypeTag> struct SolutionDependentAdvection<TypeTag, TTag::TissueTransport> { static constexpr bool value = false; };
template<class TypeTag> struct SolutionDependentMolecularDiffusion<TypeTag, TTag::TissueTransport> { static constexpr bool value = false; };
template<class TypeTag> struct SolutionDependentHeatConduction<TypeTag, TTag::TissueTransport> { static constexpr bool value = false; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::TissueTransport>
{ using type = TissueTransportProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TissueTransport>
{ using type = TissueSpatialParams<GetPropType<TypeTag, Properties::GridGeometry>, double>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TissueTransport>
{ using type = TracerFluidSystem<double>; };

// Use molar balances and a mole fraction as primary variable
template<class TypeTag>
struct UseMoles<TypeTag, TTag::TissueTransport>
{ static constexpr bool value = true; };

// effective diffusivity model
template<class TypeTag>
struct EffectiveDiffusivityModel<TypeTag, TTag::TissueTransport>
{ using type = DiffusivityConstantTortuosity<double>; };

} // end namespace Dumux::Properties


/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/// Multidomain coupling
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
namespace Dumux::Properties {

using CouplingFlow = Embedded1d3dCouplingManager<
    MultiDomainTraits<Properties::TTag::Tissue, Properties::TTag::BloodFlow>,
    Embedded1d3dCouplingMode::Average
>;

template<class TypeTag> struct CouplingManager<TypeTag, TTag::Tissue> { using type = CouplingFlow; };
template<class TypeTag> struct PointSource<TypeTag, TTag::Tissue> { using type = CouplingFlow::PointSourceTraits::template PointSource<0>; };
template<class TypeTag> struct PointSourceHelper<TypeTag, TTag::Tissue> { using type = CouplingFlow::PointSourceTraits::template PointSourceHelper<0>; };

template<class TypeTag> struct CouplingManager<TypeTag, TTag::BloodFlow> { using type = CouplingFlow; };
template<class TypeTag> struct PointSource<TypeTag, TTag::BloodFlow> { using type = CouplingFlow::PointSourceTraits::template PointSource<1>; };
template<class TypeTag> struct PointSourceHelper<TypeTag, TTag::BloodFlow> { using type = CouplingFlow::PointSourceTraits::template PointSourceHelper<1>; };

using CouplingTransport = Embedded1d3dCouplingManager<MultiDomainTraits<
    Properties::TTag::TissueTransport, Properties::TTag::BloodTransport>,
    Embedded1d3dCouplingMode::Average
>;

template<class TypeTag> struct CouplingManager<TypeTag, TTag::TissueTransport> { using type = CouplingTransport; };
template<class TypeTag> struct PointSource<TypeTag, TTag::TissueTransport> { using type = CouplingTransport::PointSourceTraits::template PointSource<0>; };
template<class TypeTag> struct PointSourceHelper<TypeTag, TTag::TissueTransport> { using type = CouplingTransport::PointSourceTraits::template PointSourceHelper<0>; };

template<class TypeTag> struct CouplingManager<TypeTag, TTag::BloodTransport> { using type = CouplingTransport; };
template<class TypeTag> struct PointSource<TypeTag, TTag::BloodTransport> { using type = CouplingTransport::PointSourceTraits::template PointSource<1>; };
template<class TypeTag> struct PointSourceHelper<TypeTag, TTag::BloodTransport> { using type = CouplingTransport::PointSourceTraits::template PointSourceHelper<1>; };

} // end namespace Dumux::Properties

#endif
