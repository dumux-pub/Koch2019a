#!/usr/bin/env python

import glob
import sys
from numpy import genfromtxt
import os
import matplotlib.pyplot as plt
plt.style.use('ggplot')

# plot the param study data
fig, axes = plt.subplots(4, 2, dpi=300, figsize=(14, 16))

font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 15}

font2 = {'family' : 'normal',
         'weight' : 'normal',
         'size'   : 15}

plt.rc('font', **font2)

pre = "healthy_"
folders = ['param_a', 'param_b', 'param_tp',
           'param_t1pre', 'param_kappaB', 'param_kappaT',
           'param_lp', 'param_d']
folders = [pre + s for s in folders]

titles = ['$a$', '$b$', '$t_p$',
           '$T_{1,pre}$', '$\kappa_B$', '$\kappa_T$',
           '$L_p$', '$D_\omega$']

idx = [(0,0), (0,1), (1,0), (1,1),
       (2,0), (2,1), (3,0), (3,1)]

# read data in dictionary
for i, (folder, title) in enumerate(zip(folders, titles)):
    data = {}
    for filename in glob.glob(os.path.join("./" + folder, '*.dat')):
        data_name = os.path.basename(filename).rstrip(".dat")
        if data_name == "mrsignal":
            continue
        date = genfromtxt(filename, delimiter=' ')
        value = float(data_name)
        data[value] = {'time':date[:,0], 'signal':date[:,1]}

    i = idx[i]
    axes[i].set_title(title, fontsize=font['size']+7, fontweight=font['weight'])
    axes[i].set_xlabel('time in s', fontsize=font['size'], fontweight=font['weight'])
    axes[i].set_ylabel('NMR signal ($S_n$)', fontsize=font['size'], fontweight=font['weight'])

    for value, d in sorted(data.items()):
        axes[i].plot(d['time'], d['signal'], '-', linewidth=5.0, label="{:.2e}".format(value))

    axes[i].set_xlim([0, 180])
    axes[i].tick_params(axis='both', which='major', labelsize=font['size'])
    axes[i].tick_params(axis='both', which='minor', labelsize=font['size'])
    axes[i].legend(loc='upper right')

fig.tight_layout(rect=[0, 0, 0.98, 1], pad=0.4, w_pad=1.0, h_pad=1.0)
fig.savefig(pre + "param_sensitivity.pdf")
#plt.show()
