#!/usr/bin/env python
import sys
import os
import duneini as dune
from numpy import genfromtxt


""""Create PEST template file variable string"""
def create_var_string(paramname, precision=40):

    return "?" + str(paramname).ljust(precision) + "?"


"""Create a PEST template file from a DuMux input file"""
def create_tpl_file(basename, params):

    inputfilename = basename + ".input"
    ini = dune.parse_ini_file(inputfilename)

    # set the parameters that should be varied
    for p in params:
        ini[p["inputname"]] = create_var_string(p["paramname"])

    filestream = open(basename + ".tpl", 'w')
    filestream.write("ptf ?\n") # make this a PEST template file
    dune.write_to_stream(ini, filestream, assignment="=")


"""Create a PEST observation file from raw patient data"""
def create_obs_file(basename):

    date = genfromtxt(basename + ".obs_raw")
    if len(date) != 80:
        raise IOError("Expected patient data with 80 data point, got {} data points".format(len(data)))

    with open(basename + ".obs", 'w') as obsfile:
        for i, s in enumerate(date):
            obsfile.write("obs{} {}\n".format(i, s))


"""Create a PEST instruction file to tell PEST how to read the observations"""
def create_ins_file(basename):

    with open(basename + ".ins", 'w') as insfile:
        insfile.write("pif *\n")
        for s in range(0,80):
            insfile.write("l1 !obs{}!\n".format(s))


"""Create a PEST config file"""
def create_pst_file(basename, params, obs):

    if len(obs) != 80:
        raise IOError("Expected patient data with 80 data point, got {} data points".format(len(obs)))

    with open(basename + ".pst", 'w') as configfile:
        configfile.write("pcf\n")

        configfile.write("* control data\n")
        configfile.write("restart estimation\n")
        configfile.write("   10    80     10      0     1       \n")
        configfile.write("    1     1 double  point     1  0  0 \n")
        configfile.write("  5.0   2.0    0.3   0.03     10      \n")
        configfile.write("  3.0   3.0  0.001      0             \n")
        configfile.write("  0.1                                 \n")
        configfile.write("   30  0.01      3      3  0.01  3    \n")
        configfile.write("    1     1      1                    \n")

        configfile.write("* singular value decomposition\n")
        configfile.write("2\n")
        configfile.write("4 5E-7\n")
        configfile.write("0\n")

        configfile.write("* parameter groups\n")
        for p in params:
            configfile.write(p["paramname"].ljust(20) + " relative 0.01 0.0 switch 2.0 parabolic\n")

        configfile.write("* parameter data\n")
        for p in params:
            configfile.write(p["paramname"].ljust(20) +
                             p["vartype"].ljust(7) + "factor " +
                             "{:.8e} ".format(p["default"]) +
                             "{:.8e} ".format(p["min"]) +
                             "{:.8e} ".format(p["max"]) +
                             p["paramname"].ljust(20) +
                             "1.0000 0.0000 1\n")

        configfile.write("* observation groups\n")
        configfile.write("obsgroup\n")

        configfile.write("* observation data\n")
        for i in range(0,80):
            weight = 1.0 # if i < 16 or i > 22 else 5.0
            configfile.write("obs{}".format(i).ljust(10) +
                             "{:.8e} ".format(obs[i]) +
                             "{:.8e} ".format(weight) +
                             "obsgroup\n")

        configfile.write("* model command line\n")
        configfile.write("./" + basename + " -Vessel.Grid.File ../../../grids/secomb/ratbrain.dgf" + "\n")

        configfile.write("* model input/output\n")
        configfile.write(basename + ".tpl " + basename + ".input" +  "\n")
        configfile.write(basename + ".ins " + " mrsignal.out" + "\n")

        configfile.write("* prior information\n")


"""Create all the necessary files for PEST"""
if __name__ == "__main__":

    if len(sys.argv) == 2:
        basename = str(sys.argv[1])
    else:
        print("Usage: ./setup_pest.py basename")
        sys.exit(1)

    # parameter configuration
    params = [
        { "inputname": "InflowCurve.a", "paramname": "inflow_a", "default": 30.0, "min": 10.0, "max": 200, "vartype": "none" },
        { "inputname": "InflowCurve.b", "paramname": "inflow_b", "default": 0.8, "min": 0.1, "max": 2, "vartype": "none" },
        { "inputname": "InflowCurve.tp", "paramname": "inflow_tp", "default": 6.0, "min": 1.0, "max": 12.0, "vartype": "none" },
        { "inputname": "VesselWall.FiltrationCoefficient", "paramname": "lp", "default": 1.0E-12, "min": 1E-12, "max": 1E-9, "vartype": "log" },
        { "inputname": "VesselWall.DiffusionCoefficient", "paramname": "diffcoeff", "default": 1E-10, "min": 1E-12, "max": 1E-5, "vartype": "log" },
        { "inputname": "MRSignal.KappaB", "paramname": "kappa_b", "default": 20.0, "min": 1.0, "max": 100.0, "vartype": "log" },
        { "inputname": "MRSignal.KappaT", "paramname": "kappa_t", "default": 1.0, "min": 0.001, "max": 100.0, "vartype": "log" },
        { "inputname": "MRSignal.T1TissuePreContrast", "paramname": "t1pre", "default": 1.0, "min": 0.8, "max": 2.0, "vartype": "none" },
        { "inputname": "MRSignal.T2TissuePreContrast", "paramname": "t2pre", "default": 3.0E-02, "min": 0.01, "max": 0.1, "vartype": "fixed" },
        { "inputname": "Problem.LesionIntensity", "paramname": "intensity", "default": 1.0, "min": 0, "max": 1, "vartype": "fixed" },
    ]

    # create PEST template file for the Dumux input file
    create_tpl_file(basename, params)

    # create PEST observation file
    # create_obs_file(basename)

    # create PEST instruction file how to read observations
    create_ins_file(basename)

    # read patient data
    obs = genfromtxt(basename + ".obs_raw")

    # create PEST config file
    create_pst_file(basename, params, obs)
