#!/usr/bin/env python3
import sys
import os
import glob
import matplotlib.pyplot as plt
from numpy import genfromtxt
from numpy import mean
import numpy as np

t = [i*1.4 for i in range(0,80)]

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 20}

font2 = {'family' : 'normal',
         'weight' : 'normal',
         'size'   : 20}

plt.rc('font', **font2)

if len(sys.argv) == 4:
    simname = str(sys.argv[1])
    obsname = str(sys.argv[2])
    colorscheme = str(sys.argv[3])
    sim = genfromtxt(simname)
    obs = genfromtxt(obsname)

    colors = []
    if colorscheme == "blue":
        colors.append("#2ea0a0ff")
        colors.append("#2ea0a0ff")
    elif colorscheme == "red":
        colors.append("#e96781ff")
        colors.append("#e96781ff")
    else:
        raise ValueError("Unknown color scheme {}".format(colorscheme))

    print("Relative L2-error: ", np.linalg.norm(sim-obs)/np.linalg.norm(sim))

    fig = plt.figure(dpi=300, figsize=(8, 5))

    plt.ylabel('NMR signal \n (normalized to baseline)', fontsize=font['size'], fontweight=font['weight'])
    plt.xlabel('time in s', fontsize=font['size'], fontweight=font['weight'])

    plt.plot(t, sim, "-", color=colors[0], linewidth=4.0, label="simulation")
    plt.plot(t, obs, "-", color=colors[1], linewidth=2.0, marker="+", markersize=5, mew=5, label="data")
    ax = plt.gca()
    ax.tick_params(axis='both', which='major', labelsize=16)
    ax.tick_params(axis='both', which='minor', labelsize=16)

    plt.xlim([0, 120])
    plt.legend(loc='upper left')
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0, rect=[0.05, 0, 1, 1])
    fig.savefig("best_fit.pdf")
    fig.savefig("best_fit.png")
    plt.show()

elif len(sys.argv) == 1:
    simname_h = "healthy/mrsignal.out"
    simname_s = "sick/mrsignal.out"
    obsname_h = "healthy/msbern.obs_raw"
    obsname_s = "sick/msbern.obs_raw"
    colors = ["#2ea0a0ff", "#e96781ff"]
    sim_h = genfromtxt(simname_h)
    sim_s = genfromtxt(simname_s)
    obs_h = genfromtxt(obsname_h)
    obs_s = genfromtxt(obsname_s)

    print("Relative L2-error (NAWM): ", np.linalg.norm(sim_h-obs_h)/np.linalg.norm(sim_h))
    print("Relative L2-error (lesion): ", np.linalg.norm(sim_s-obs_s)/np.linalg.norm(sim_s))
    print("L2-error (NAWM): ", np.linalg.norm(sim_h-obs_h))
    print("L2-error (lesion): ", np.linalg.norm(sim_s-obs_s))

    fig, axes = plt.subplots(1, 2, dpi=300, figsize=(12, 4))

    for ax in axes:
        ax.set_ylabel('norm. NMR signal', fontsize=font['size'], fontweight=font['weight'])
        ax.set_xlabel('time in s', fontsize=font['size'], fontweight=font['weight'])

    axes[0].plot(t, sim_s, "-", color=colors[1], linewidth=4.0, label="simulation")
    axes[0].plot(t, obs_s, "-", color=colors[1], linewidth=2.0, marker="+", markersize=5, mew=5, label="lesion data")
    axes[1].plot(t, sim_h, "-", color=colors[0], linewidth=4.0, label="simulation")
    axes[1].plot(t, obs_h, "-", color=colors[0], linewidth=2.0, marker="+", markersize=5, mew=5, label="NAWM data")
    for ax in axes:
        ax.tick_params(axis='both', which='major', labelsize=16)
        ax.tick_params(axis='both', which='minor', labelsize=16)
        ax.set_xlim([0, 120])
        ax.legend(loc='lower right')

    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0, rect=[0.01, 0, 1, 1])
    fig.savefig("best_fit.pdf")
    fig.savefig("best_fit.png")
    plt.show()

else:
    raise IOError("Provide simname, obsname, colorscheme (red/blue)")
